FROM nginx:mainline-alpine
COPY ./config/certs/fullchain.snakeoil.pem /etc/letsencrypt/live/${DOMAIN_NAME}/fullchain.pem
COPY ./config/certs/privkey.snakeoil.pem /etc/letsencrypt/live/${DOMAIN_NAME}/privkey.pem

COPY ./scripts/autoreload-nginx /usr/local/bin/autoreload-nginx
COPY ./scripts/conf.d-inotifyd /etc/conf.d/inotifyd
COPY ./scripts/init.d-inotifyd /etc/init.d/inotifyd

RUN apk update && apk upgrade --no-cache
