FROM node:lts-alpine
COPY ./src /app
WORKDIR /app
RUN npm install sails -g
RUN npm install
CMD { 'npm', 'start'}
