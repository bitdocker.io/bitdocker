user       root root;
worker_processes  2;
error_log  /var/log/nginx/error.log;
pid        /var/log/nginx/nginx.pid;
worker_rlimit_nofile 8192;

events {
  worker_connections  4096;  ## Default: 1024
}

http {
  include    mime.types;
  index    index.html index.htm;
  charset UTF-8;

  default_type application/octet-stream;
  log_format   main '$remote_addr - $remote_user [$time_local]  $status '
    '"$request" $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';
  access_log   /var/log/nginx/access.log  main;
  sendfile     on;
  tcp_nopush   on;
  tcp_nodelay  on;
  keepalive_timeout  90;
  server_names_hash_bucket_size 128; # this seems to be required for some vhosts

  set_real_ip_from 103.21.244.0/22;
  set_real_ip_from 103.22.200.0/22;
  set_real_ip_from 103.31.4.0/22;
  set_real_ip_from 104.16.0.0/12;
  set_real_ip_from 108.162.192.0/18;
  set_real_ip_from 131.0.72.0/22;
  set_real_ip_from 141.101.64.0/18;
  set_real_ip_from 162.158.0.0/15;
  set_real_ip_from 172.64.0.0/13;
  set_real_ip_from 173.245.48.0/20;
  set_real_ip_from 188.114.96.0/20;
  set_real_ip_from 190.93.240.0/20;
  set_real_ip_from 197.234.240.0/22;
  set_real_ip_from 198.41.128.0/17;
  set_real_ip_from 2400:cb00::/32;
  set_real_ip_from 2606:4700::/32;
  set_real_ip_from 2803:f800::/32;
  set_real_ip_from 2405:b500::/32;
  set_real_ip_from 2405:8100::/32;
  set_real_ip_from 2c0f:f248::/32;
  set_real_ip_from 2a06:98c0::/29;
  # set_real_ip_from 192.168.1.1;
  set_real_ip_from 127.0.0.1; #x.x.x.x is your proxy IP

  # Remove X-Powered-By and server token, which is an information leak
  fastcgi_hide_header X-Powered-By;
  server_tokens off;

  ##
  # SSL Settings
  ##
  
  ssl_prefer_server_ciphers on;
  ssl_protocols TLSv1.2 TLSv1.3; # Requires nginx >= 1.13.0 else use TLSv1.2
  ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
  
  ssl_dhparam /etc/nginx/dhparam.pem; # openssl dhparam -out /etc/nginx/dhparam.pem 4096
  ssl_ecdh_curve secp384r1;
  
  ssl_session_timeout 1d;
  ssl_session_cache shared:justID:20m;  # about 40000 sessions
  ssl_session_tickets off;
  
  ssl_stapling on; 
  ssl_stapling_verify on;
  
  
  ## DNS Resolver
  resolver 1.1.1.1 1.0.0.1 valid=300s;
  resolver_timeout 3s; 

  ## Rate limiting
  limit_req_zone $binary_remote_addr zone=api:10m rate=20r/s;

  server {
    listen       80;
    server_name  cloud.just.id;

    # enforce https
    return 301 https://$server_name$request_uri;

  }

  server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name  cloud.just.id;

    ssl_certificate /etc/letsencrypt/live/$server_name/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$server_name/privkey.pem;

    # Add headers to serve security related headers
    # Before enabling Strict-Transport-Security headers please read into this
    # topic first.
    # add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;" always;

    # add_header Referrer-Policy "no-referrer" always;
    add_header X-Content-Type-Options "nosniff" always;
    add_header X-Download-Options "noopen" always;
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header X-Permitted-Cross-Domain-Policies "none" always;
    add_header X-Robots-Tag "none" always;
    add_header X-XSS-Protection "1; mode=block" always;

    # set max upload size
    client_max_body_size 512M;

    # pass requests for dynamic content to rails/turbogears/zope, et al

    location /api {
      limit_req zone=api burst=30 nodelay;
      proxy_cache_bypass 1;

      proxy_pass      http://sails:1337;
      proxy_http_version 1.1;
      proxy_set_header   X-Forwarded-For $remote_addr;
      proxy_set_header   Host $http_host;
      proxy_set_header   X-Real-IP $remote_addr;
      proxy_set_header   X-Forwarded-Proto $scheme;
    }

    # Websocket
    location /socket.io {
      proxy_cache_bypass 1;

      proxy_pass http://sails:1337;
      proxy_http_version 1.1;
      proxy_set_header   X-Forwarded-For $remote_addr;
      proxy_set_header   Host $http_host;
      proxy_set_header   Upgrade $http_upgrade;
      proxy_set_header   Connection "upgrade";
    }

    location / {
      proxy_pass      http://sails:1337;
      proxy_http_version 1.1;
      proxy_set_header   X-Forwarded-For $remote_addr;
      proxy_set_header   Host $http_host;
      proxy_set_header   X-Real-IP $remote_addr;
      proxy_set_header   X-Forwarded-Proto $scheme;
    }
  }

}