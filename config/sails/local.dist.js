/* eslint-disable camelcase */
/**
 * Local environment settings
 *
 * Use this file to specify configuration settings for use while developing
 * the app on your personal system.
 *
 * For more information, check out:
 * https://sailsjs.com/docs/concepts/configuration/the-local-js-file
 */

module.exports = {

  // Any configuration settings may be overridden below, whether it's built-in Sails
  // options or custom configuration specifically for your app (e.g. Stripe, Sendgrid, etc.)

  custom: {
    baseUrl: 'https://bitdocker.io',

    // The sender that all outgoing emails will appear to come from.
    fromEmailAddress: 'no.reply@bitdocker.io',
    fromName: 'BitDocker Team',

    // Email address for receiving support messages & other correspondences.
    // > If you're using the default privacy policy, this will be referenced
    // > as the contact email of your "data protection officer" for the purpose
    // > of compliance with regulations such as GDPR.
    internalEmailAddress: 'support@bitdocker.io',

    // Whether to require proof of email address ownership any time a new user
    // signs up, or when an existing user attempts to change their email address.
    verifyEmailAddresses: true,



    // Credentials
    b2KeyName: 'key-name-123',
    b2KeyId: 'xxxxxxxxxxxxx',
    b2Key: 'K002Xxxxxxxxxxxxxxxxxxxx',
  
    bucketName: 'some-bucket-name',
    bucketId: 'xxxxxxxxxxxxxxxxxxxx',
    bucketUrl: 'https://f002.backblazeb2.com',
    bucketApiUrl: 'https://api.backblazeb2.com',

    b2FileNamePrefix: 'user/',
    downloadUrl: 'https://dl.bitdocker.io',
    uploadUrl: 'https://up.bitdocker.io',

    defaultDownloadTokenValidSeconds: 3600,   // 1 hour
  
    namespace: {
      user: 'NAMESPACE1',
      file: 'NAMESPACE2',
      fodler: 'NAMESPACE3',
    },

    // For signing filenames and paths
    jwtSigningSecret: 'SECRET',
    cryptoKey: 'KEY',

    ssoBaseUrl: 'https://just.id',

    justIdPubKey: `-----BEGIN PUBLIC KEY-----
MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEenvJYxX9+iHs4LcwxlNO+KTIWx2r4mTl
gQm4t0Dz1uNOi4wau04FepqTjPDK60F9mmBegRNPqUX2Cbx4O02xmS02bVEsYeBK
tuZRVsAyX9isinbAH4yRbk83bNb+DvCY
-----END PUBLIC KEY-----`,

    redis: {
      url: 'redis://redis:6379/16',
      password: 'SECRET',
    }
  },

  
  datastores: {
    default: {
      adapter: 'sails-mongo',
      url: 'mongodb://mongo:27017/cloud',
      ssl: false,
    },
  },

  models: {
    migrate: 'safe',
    // cascadeOnDestroy: false,

    dataEncryptionKeys: {
      default: 'SECRET'
    }
  },

  security: {
    cors: {
      allRoutes: false,
      allowOrigins: 'https',
      allowCredentials: false,
    },

    csrf: true,
  },

  session: {
    adapter: 'connect-mongo',
    url: 'mongodb://mongo:27017/session',
    secret: 'SECRET',

    cookie: {
      secure: true,
      maxAge: 24 * 60 * 60 * 1000,  // 24 hours
    },
  },

  sockets: {
    onlyAllowOrigins: [
      'https://bitdocker.io',
    ],
  },

  log: {
    level: 'debug'
  },

  http: {
    cache: 7 * 24 * 60 * 60 * 1000, // 7 Days
    trustProxy: true,
  },


};
