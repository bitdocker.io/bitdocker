addEventListener('fetch', event => {

  const request = event.request
  const url = new URL(request.url)

  if (request.method === "OPTIONS") {
    // Handle CORS preflight requests
    event.respondWith(handleOptions(request))
  }
  else if(
    request.method === "GET" ||
    request.method === "HEAD"
  ){
    // Handle requests to the API server
    event.respondWith(handleRequest(request))
  }
  else {
    event.respondWith(
      new Response(null, {
        status: 405,
        statusText: "Method Not Allowed",
      }),
    )
  }

})

async function handleRequest(request) {

  let url = new URL(request.url);

  if (!url.search.startsWith('?Authorization=')){
    return new Response(null, {
      status: 401,
      statusText: 'Unauthorized'
    });
  }

  // Using RegExp because url.pathname partially decodes the pathname
  var regex = new RegExp(/(\/user\/[0-9a-z-]{36}\/.{1,})\?/, 'g');
  const pathnameMatch = regex.exec(request.url);

  if (!pathnameMatch){
    return new Response(null, {
      status: 404,
      statusText: 'Not Found'
    });
  }

  // First matching group is the pathname
  const pathname = pathnameMatch[1];

  let newUrl = 'https://f002.backblazeb2.com/file/bitdocker' + pathname + url.search;

  const modRequest = new Request(newUrl, {
    body: request.body,
    headers: request.headers,
    method: request.method,
  });

  let response = await fetch(modRequest)


  response = new Response( response.body, response)
  response.headers.set("Access-Control-Allow-Origin", "*");
  response.headers.set("Access-Control-Allow-Headers", "*");
  response.headers.set("Access-Control-Allow-Methods", "GET, HEAD, OPTION");
  response.headers.set("Access-Control-Max-Age", 86400);
  response.headers.set("Content-Disposition", "attachment");

  return response
}

function handleOptions(request) {
  // Make sure the necessary headers are present
  // for this to be a valid pre-flight request

  let headers = request.headers;

  if (
    headers.get("Origin") !== null &&
    headers.get("Access-Control-Request-Method") !== null &&
    headers.get("Access-Control-Request-Headers") !== null
  ){

    // Handle CORS pre-flight request.
    // If you want to check or reject the requested method + headers
    // you can do that here.

    let respHeaders = {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Methods": [ "GET", "OPTIONS", "HEAD"],
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Max-Age": 86400,
    }
    
    
    return new Response(null, {
      headers: respHeaders,
    })

  }  else {

  // Handle standard OPTIONS request.
  // If you want to allow other HTTP Methods, you can do that here.

  return new Response(null, {
    headers: {
      Allow: "GET, HEAD, OPTIONS",
    },
  })

}}
