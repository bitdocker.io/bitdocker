const jwt = require('@tsndr/cloudflare-worker-jwt');
const crypto = require('crypto');

addEventListener('fetch', event => {

  const request = event.request;
  const url = new URL(request.url);

  if (request.method === "OPTIONS") {
    // Handle CORS preflight requests
    event.respondWith(handleOptions(request));
  }
  else if(
    request.method === "POST" ||
    request.method === "HEAD"
  ){
    // Handle requests to the API server
    event.respondWith(handleRequest(request));
  }
  else {
    event.respondWith(
      new Response(null, {
        status: 405,
        statusText: "Method Not Allowed",
      }),
    )
  }

})

async function handleRequest(request) {

  let uploadUrl = request.headers.get('x-io-uploadurl');
  if (!uploadUrl || uploadUrl.length <= 0) return badRequest();

  let authorizationToken = '';

  let encryptedSignature = request.headers.get('x-io-signature');
  if (!encryptedSignature || encryptedSignature.length <= 0){
    return unauthorized();
  } else {

    // IV
    const resizedIV = Buffer.allocUnsafe(16);
    const iv = crypto
      .createHash('sha256')
      .update(crypto.randomBytes(16))
      .digest();

    iv.copy(resizedIV);

    //Decrypt signature
    const key = crypto
    .createHash('sha256')
    .update(CRYPTO_KEY)
    .digest();
    const decipher = crypto.createDecipheriv('aes256', key, resizedIV);


    let signature = decipher.update(encryptedSignature, 'base64', 'utf8');
    signature += decipher.final('utf8');

    // Verify signature
    try{
      const isValid = jwt.verify(signature, JWT_SECRET);
      if (!isValid){
        return unauthorized();
      }
  
      // If signature is valid, verify filename
      const payload = jwt.decode(signature);
      const fileNameChecksum = payload.fileNameChecksum;
      authorizationToken = payload.authorizationToken;
  
      const checksum = crypto.createHash('sha256').update(request.headers.get('X-Bz-File-Name')).digest('hex');
  
      if (!fileNameChecksum || !authorizationToken) return badRequest();
      if (checksum !== fileNameChecksum) return unauthorized();
    } catch(err){
      return unauthorized();
    }

  }


  let newUrl = new URL(uploadUrl);
  let newHeaders = new Headers(request.headers);
  newHeaders.append('Authorization', authorizationToken);
  newHeaders.delete('x-io-signature');
  newHeaders.delete('x-io-uploadurl');
  const modRequest = new Request(newUrl, {
    body: request.body,
    headers: newHeaders,
    method: request.method,
  });

  let response = await fetch(modRequest)

  response = new Response( response.body, response)
  response.headers.set("Access-Control-Allow-Origin", request.headers.get("Origin"));
  response.headers.set("Access-Control-Allow-Headers", "*");
  response.headers.set("Access-Control-Allow-Methods", "POST, HEAD, OPTION");
  response.headers.set("Access-Control-Max-Age", 86400);
  response.headers.append("Vary", "Origin")

  return response
}

function handleOptions(request) {
  // Make sure the necessary headers are present
  // for this to be a valid pre-flight request

  let headers = request.headers;

  if (
    headers.get("Origin") !== null
    && headers.get("Access-Control-Request-Method") !== null
    && headers.get("Access-Control-Request-Headers") !== null
  ){

    // Handle CORS pre-flight request.
    // If you want to check or reject the requested method + headers
    // you can do that here.

    // "Access-Control-Allow-Headers": ["Accept", "Content-Type", "User-Agent", "X-Bz-Content-Sha1", "X-Bz-File-Name", "X-Bz-Info-src_last_modified_millis", "x-io-signature", "x-io-uploadurl"]

    let respHeaders = {
      "Access-Control-Allow-Origin": request.headers.get("Origin"),
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Methods": [ "POST", "OPTIONS", "HEAD"],
      "Access-Control-Max-Age": 86400,
      "Vary": "Origin"
    };
    
    
    return new Response(null, {
      headers: respHeaders,
    });

  }  else {

    // Handle standard OPTIONS request.
    // If you want to allow other HTTP Methods, you can do that here.

    return new Response(null, {
      headers: {
        Allow: "POST, HEAD, OPTIONS",
      },
    });

  }

}

function unauthorized(){
  return new Response(null, {
    status: 401,
    statusText: 'Unauthorized'
  });
}

function badRequest(){
  return new Response(null, {
    status: 400,
    statusText: 'Bad Request'
  });
}
