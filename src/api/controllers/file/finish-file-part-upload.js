module.exports = {


  friendlyName: 'Finish file part upload',


  description: '',


  inputs: {

    fileId: {
      type: 'string',
      required: true,
      maxLength: 36,
      regex: /^[a-z0-9]+$/,
    },

    sha1: {
      type: 'string',
      required: true,
      minLength: 40,
      maxLength: 40,
      regex: /^[a-z0-9]+$/,
    },

    partNumber: {
      type: 'number',
      inInteger: true,
      required: true,
      min: 1,
      max: 10000,
    },


  },


  exits: {

  },


  fn: async function (inputs) {

    // Mark file uploaded
    await FileObjectPart.updateOne({
      where: {
        fileObject: inputs.fileId,
        sha1: inputs.sha1,
        partNumber: inputs.partNumber,
      }
    })
    .set({
      action: 'upload',
    });
    // .exec( (err) => {
    //   sails.log.error(err);
    // });

    let count = await FileObjectPart.count({
      where: {
        fileObject: inputs.fileId,
        action: 'upload'
      }
    });

    // All done.
    return {
      upload: count
    };

  }


};
