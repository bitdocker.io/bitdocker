module.exports = {


  friendlyName: 'Complete file upload',


  description: '',


  inputs: {

    fileId: {
      type: 'string',
      required: true,
      maxLength: 36,
      regex: /^[a-z0-9]+$/,
    },

    b2FileId: {
      type: 'string',
      required: true,
      isNotEmptyString: true,
    },

    sha1: {
      type: 'string',
      required: true,
      minLength: 40,
      maxLength: 40,
      regex: /^[a-z0-9]+$/,
    },

    fileInfo: {
      type: 'string',
      description: 'Stringified JSON',
      maxLength: 512,
    },

    contentType: {
      type: 'string',
      description: 'The MIME type of the content of the file.',
      maxLength: 64,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound',
      description: 'File not found.',
    },

    badRequest: {
      responseType: 'badRequest',
      desccription: 'Invalid parameters.',
    },

    serverError: {
      responseType: 'serverError',
      description: 'Error processing record.',
    }

  },


  fn: async function (inputs) {

    let fileInfo = null;

    if (inputs.fileInfo){
      try{
        fileInfo = JSON.parse(inputs.fileInfo);
      } catch(err){
        if (err){
          sails.log.error(err);
        }
        throw 'badRequest';
      }
    }

    let params = _.extend({
      action: 'upload',
      b2FileId: inputs.b2FileId,
      fileInfo: fileInfo,
      uploadTimestamp: Date.now(),
    },
    inputs.contentType?
    {
      contentType: inputs.contentType
    }
    : {});

    // Mark file uploaded
    
    await FileObject.updateOne({
      where: {
        id: inputs.fileId,
        sha1: inputs.sha1,
        user: this.req.me.id,
      }
    })
    .set(params)
    .intercept( (err) => {
      sails.log.error(err.message);
      throw 'serverError';
    });

    // Add file

    // All done.
    return;

  }


};
