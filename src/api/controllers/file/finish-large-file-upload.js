module.exports = {


  friendlyName: 'Finish large file upload',


  description: '',


  inputs: {

    fileId: {
      type: 'string',
      required: true,
      maxLength: 36,
      regex: /^[a-z0-9]+$/,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound',
      description: 'File not found.',
    },

    badRequest: {
      responseType: 'badRequest',
      description: 'Not all parts are uploaded.',
    },

    serverError: {
      responseType: 'serverError',
      description: 'Error processing request.',
    }

  },


  fn: async function (inputs) {

    let file = await FileObject.findOne({
      where: {
        id: inputs.fileId,
        user: this.req.me.id,
      }
    }).populate('fileObjectParts', {
      sort: 'partNumber ASC'
    });

    if (!file){ throw 'notFound'; }

    FileObject.updateOne({
      where: {
        id: file.id,
      }
    })
    .set({
      action: 'finish',
    })
    .exec( (err) => {
      if (err){
        sails.log.error(err);
      }
    });

    // Post B2 server to finish file upload
    let axios = require('axios');
    let credentials = await sails.helpers.getAuthorizationToken();

    let parts = file.fileObjectParts;
    let sha1Array = [];
    let sha1String = '';
    for (let i=0; i < parts.length; i++){
      if (parts[i].action !== 'upload'){ throw 'badRequest';}
      sha1Array.push(parts[i].sha1);
      sha1String += parts[i].sha1;
    }

    console.log(sha1Array);

    let res = await axios({
      method: 'POST',
      url: credentials.apiUrl + '/b2api/v1/b2_finish_large_file',
      headers: {
        'Authorization': credentials.authorizationToken,
        'User-Agent': sails.config.custom.apiUserAgent,
      },
      data: {
        'fileId': file.b2FileId,
        'partSha1Array': sha1Array,
      }
    }).catch( (err) => {
      if (err){
        sails.log.error(err);

        throw 'serverError';
      }
    });


    const hashSha1 = require('sha1');
    const sha1Checksum = hashSha1(sha1String);

    // console.log(res.data);
    FileObject.updateOne({
      where: {
        id: file.id,
      }
    }).set({
      action: 'upload',
      sha1: sha1Checksum,
      partsSha1: sha1Array,
    })
    .exec( (err) => {
      if (err){
        sails.log.error(err);
      }
    });


    // Delete parts
    FileObjectPart.destroy({
      where: {
        fileObject: file.id,
      }
    })
    .exec( (err) => {
      if (err){
        sails.log.error(err);
      }
    });


    return {fileId: file.id};

  }


};
