module.exports = {


  friendlyName: 'Get download parameters',


  description: '',


  inputs: {

  },


  exits: {

    notFound: {
      responseType: 'notFound',
    },

    badRequest: {
      responseType: 'badRequest',
    },

  },


  fn: async function (inputs) {

    let fileId = this.req.query.fileId;
    if (!fileId || fileId === ''){ throw 'notFound'; }

    let fileCount = await FileObject.count({
      where: {
        id: fileId,
        user: this.req.me.id,
      }
    });

    if (fileCount < 1){ throw 'notFound'; }

    // Find existing download tokens

    let token = await DownloadToken.find({
      where: {
        fileId: fileId,
        createdAt: { '>' : Date.now() - 900000},   // 15 mins
      },
      limit: 1
    });

    let downloadParams = {};

    if (token && token.length > 0){
      token = token[0];

      downloadParams = {
        authorizationToken: token.authorizationToken,
        fileNamePrefix: token.fileNamePrefix,
      };

    } else {

      let params = {
        fileId: fileId,
        userUuid: this.req.me.uuid,
      };

      let res = await sails.helpers.b2GetDownloadAuthorization.with(params);

      console.log(res);

      await DownloadToken.create({
        fileId: fileId,
        user: this.req.me.id,
        fileNamePrefix: res.fileNamePrefix,
        authorizationToken: res.authorizationToken,
      });

      console.log('Got new download token.');

      downloadParams = {
        authorizationToken: res.authorizationToken,
        fileNamePrefix: res.fileNamePrefix,
      };
    }


    let res = {
      downloadUrl: sails.config.custom.downloadUrl + '/' + downloadParams.fileNamePrefix,
      authorizationToken: downloadParams.authorizationToken,
    };

    // All done.
    return res;

  }


};
