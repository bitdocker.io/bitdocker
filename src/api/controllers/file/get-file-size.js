module.exports = {


  friendlyName: 'Get file size',


  description: '',


  inputs: {

    fileIds: {
      type: 'json',
      required: true,
      description: 'A JSON object containing an array of file IDs.'
    }

  },


  exits: {

    usageError: {
      responseType: 'usageError',
      description: 'Number of files cannot be more than 100 at a time.'
    },

    badRequest: {
      responseType: 'badRequest',
    }

  },


  fn: async function (inputs) {

    if (!this.req.isSocket){
      throw 'badRequest';
    }

    const ids = inputs.fileIds;

    if (!ids || !ids.length){ return 0; }
    if (ids.length > 100){ throw 'usageError'; }

    let size = 0;
    let count = 0;

    for (let i=0; i < ids.length; i++){
      let file = await FileObject({
        where: {
          id: ids[i],
          user: this.req.me.id,
        },
        select: ['id', 'contentLength'],
      });

      if (file){
        size += file.contentLength;
        count += 1;
      }

    }

    // All done.
    return {count, size};

  }


};
