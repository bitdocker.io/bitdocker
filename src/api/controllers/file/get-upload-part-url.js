module.exports = {


  friendlyName: 'Get upload part url',


  description: '',


  inputs: {

    fileId: {
      type: 'string',
      required: true,
      maxLength: 36,
      regex: /^[a-z0-9]+$/,
    },

    contentLength: {
      type: 'number',
      description: 'Size of part',
      isInteger: true,
      required: true,
      min: 0,
      max: 5000000000,  // 5GB max per part
    },

    sha1: {
      type: 'string',
      description: 'SHA-1 checksums of the part',
      example: '2346ad27d7568ba9896f1b7da6b5991251debdf2',
      required: true,
      minLength: 40,
      maxLength: 40,
      regex: /^[a-z0-9]+$/,
    },

    partNumber: {
      type: 'number',
      description: 'The number of part',
      isInteger: true,
      required: true,
      min: 1,
      max: 10000,
      example: 8,
    },


  },


  exits: {
    serverError: {
      responseType: 'serverError',
      description: 'Error communicating with B2 server.',
    },

    folderNotFound: {
      responseType: 'notFound',
      description: 'Folder not found.',
    },

    badRequest: {
      responseType: 'badRequest',
      description: 'File name invalid.',
    },

    partExists: {
      responseType: 'badRequest',
      description: 'Part already uploaded.',
    }
  },


  fn: async function (inputs) {

    // Get file info
    var file = await FileObject.findOne({
      where: {
        id: inputs.fileId,
        user: this.req.me.id,
      }
    });

    if (!file){
      throw 'notFound';
    }

    // TODO: check if any parent folder is marked for removal. If true, throw error.

    // Check if the part is already uploaded
    let filePart = await FileObjectPart.findOne({
      where: {
        sha1: inputs.sha1,
        partNumber: inputs.partNumber,
        contentLength: inputs.contentLength,
        fileObject: file.id,
      }
    });

    if (filePart && filePart.action === 'upload'){
      throw 'partExists';
    }

    // Obtain upload URL
    let axios = require('axios');
    let credentials = await sails.helpers.getAuthorizationToken();

    let res = await axios({
      method: 'POST',
      url: credentials.apiUrl + '/b2api/v1/b2_get_upload_part_url',
      headers: {
        'Authorization': credentials.authorizationToken,
        'User-Agent': sails.config.custom.apiUserAgent,
      },
      data: {
        'fileId': file.b2FileId,
      }
    }).catch( (err) => {
      if (err){
        sails.log.error(err);
      }
    });

    if (!res.data){
      console.log('Error getting upload part URL. Abort.');
      throw 'serverError';
    }


    if (!filePart){
      // Add part to collection
      FileObjectPart.create({
        sha1: inputs.sha1,
        b2FileId: res.fileId,
        partNumber: inputs.partNumber,
        contentLength: inputs.contentLength,
        fileObject: file.id,
      }).fetch().exec( (err, res) => {
        if (err){
          sails.log.error(err);
        }

        FileObject.addToCollection( res.fileObject, 'fileObjectParts')
        .members(res.id)
        .exec( (err) => {
          if (err){
            sails.log.error(err);
          }
        });
      });
    }

    let uploadParams = {
      fileId: file.id,
      b2FileId: res.data.fileId,
      uploadUrl: res.data.uploadUrl,
      authorizationToken: res.data.authorizationToken,
    };

    // console.log(uploadParams);

    // All done.
    return uploadParams;

  }


};
