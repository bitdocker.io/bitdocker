module.exports = {


  friendlyName: 'Get upload url',


  description: '',


  inputs: {

    fileName: {
      type: 'string',
      description: 'Fully encoded filename',
      required: true,
      maxLength: 765,
    },

    folderId: {
      type: 'string',
      required: true,
      maxLength: 36,
      regex: /^[a-z0-9]+$/,
    },

    path: {
      type: 'string',
      description: 'Fully encoded folder path',
      required: true,
      maxLength: 1024,
    },

    contentType: {
      type: 'string',
      description: 'The MIME type of the content of the file.',
      maxLength: 64,
    },

    sha1: {
      type: 'string',
      description: 'SHA-1 checksums of the file',
      example: '2346ad27d7568ba9896f1b7da6b5991251debdf2',
      required: true,
      minLength: 40,
      maxLength: 40,
      regex: /^[a-z0-9]+$/,
    },

    contentLength: {
      type: 'number',
      desciption: 'Size of the simple file',
      isInteger: true,
      min: 0,
      max: 100000000, // 100MB max per simple file
    },

    fileInfo: {
      type: 'string',
      description: 'Stringified JSON',
      maxLength: 512,
    }

  },


  exits: {

    serverError: {
      responseType: 'serverError',
      description: 'Error communicating with external server.',
    },

    folderNotFound: {
      responseType: 'notFound',
      description: 'Folder not found.',
    },

    badFileInfo: {
      responseType: 'badRequest',
      description: 'File info invalid.',
    },

    fileNameInvalid: {
      responseType: 'badRequest',
      description: 'File name invalid',
    },

    pathInvalid: {
      responseType: 'badRequest',
      description: 'Path invalid',
    },

    fileExists: {
      responseType: 'badRequest',
      description: 'File already exists.',
    }

  },


  fn: async function (inputs) {

    const jwt = require('jsonwebtoken');
    const crypto = require('crypto');

    let checkFilename = await sails.helpers.checkFilename(inputs.fileName);
    if (!checkFilename){ throw 'fileNameInvalid'; }

    let checkPath = await sails.helpers.checkPath(inputs.path);
    if (!checkPath){ throw 'pathInvalid'; }

    const sanitizedName = inputs.fileName;
    const sanitizedPath = inputs.path;

    // If fileInfo is not empty, check for syntax errors.
    let fileInfo = null;

    if (inputs.fileInfo)
    {
      try {
        fileInfo = JSON.parse(inputs.fileInfo);
      } catch(err){
        if (err){
          throw 'badFileInfo';
        }
      }
    }


    // Get current folder info

    let folder = await FolderObject.findOne({
      where: {
        id: inputs.folderId,
        user: this.req.me.id,
      }
    });

    if (!folder){
      throw 'notFound';
    }

    // TODO: check if any parent folder is marked for removal. If true, throw error.


    // Do all the required folders exist?
    let targetFolder = {};

    if (sanitizedPath === ''){
      targetFolder = folder;
    } else {

      // Search for folder directly

      targetFolder = await FolderObject.findOne({
        where: {
          user: this.req.me.id,
          path: sanitizedPath
        }
      });

      if (!targetFolder){

        let path = sanitizedPath.substring(folder.path.length-1, sanitizedPath.length);

        sails.log.debug(path);

        targetFolder = await sails.helpers.createFoldersRecursive.with({ userId: this.req.me.id, baseFolder: folder, path: path});

      } else {
        // sails.log.debug('Found folder by path.');
      }



    }

    sails.log.debug('Target folder path: ' + targetFolder.path );


    // Is there an existing file with the same name?

    let fileSearch = await FileObject.findOne({
      where: {
        parentFolder: targetFolder.id,
        fileName: sanitizedName,
        user: this.req.me.id,
      }
    });

    if (fileSearch && fileSearch.action === 'upload'){ throw 'fileExists'; }
    else if (fileSearch){

      FileObject.destroyOne({
        where: {
          id: fileSearch.id
        }
      })
      .exec( (err) => {
        if (err){
          sails.log.error(err);
          sails.log.error('Error removing existing file record.');
        }
      });
    }


    // Obtain upload URL
    let axios = require('axios');
    let credentials = await sails.helpers.getAuthorizationToken();

    if (!inputs.contentType || inputs.contentType.length === 0){
      inputs.contentType = 'b2/x-auto';
    }

    let res = await axios({
      method: 'get',
      url: credentials.apiUrl + '/b2api/v1/b2_get_upload_url',
      responseType: 'json',
      headers: {
        'Authorization': credentials.authorizationToken,
        'User-Agent': sails.config.custom.apiUserAgent,
      },
      params: {
        'bucketId': sails.config.custom.bucketId,
      }
    }).catch( (err) => {
      sails.log.error(err);
      sails.log.error('Error calling API b2_get_upload_url');
    });

    if (!res.data){
      sails.log.error('Error getting upload URL. Abort.');
      throw 'serverError';
    }



    let file = await FileObject.create({
      user: this.req.me.id,
      parentFolder: targetFolder.id,
      fileName: sanitizedName,
      contentType: inputs.contentType,
      contentLength: inputs.contentLength,
      sha1: inputs.sha1,
      fileInfo: fileInfo,
      parts: 1,
    }).fetch();

    // Add file to folder

    await FolderObject.addToCollection(targetFolder.id, 'fileObjects').members(file.id);

    // Add to user file collection
    await User.addToCollection(this.req.me.id, 'fileObjects').members(file.id);

    // let user = await User.findOne({ id: this.req.me.id }).populate('fileObjects');
    // console.log(user);

    const fileName = sails.config.custom.b2FileNamePrefix + this.req.me.uuid + targetFolder.path + sanitizedName;

    const signData = {
      exp: Math.floor((Date.now() + 86400000)/1000),
      authorizationToken: res.data.authorizationToken,
      fileNameChecksum: crypto.createHash('sha256').update(fileName).digest('hex')
    };
    const signature = jwt.sign(signData, sails.config.custom.jwtSigningSecret, {algorithm: 'HS256'});

    // console.log(signature);

    // IV
    const resizedIV = Buffer.allocUnsafe(16);
    const iv = crypto
      .createHash('sha256')
      .update(crypto.randomBytes(16))
      .digest();

    iv.copy(resizedIV);

    //Encrypt signature
    const key = crypto
    .createHash('sha256')
    .update(sails.config.custom.cryptoKey)
    .digest();
    const cipher = crypto.createCipheriv('aes256', key, resizedIV);

    let encryptedJWT = cipher.update(signature, 'utf8', 'base64');
    encryptedJWT += cipher.final('base64');

    let uploadParams = {
      fileId: file.id,
      folderId: targetFolder.id,
      uploadUrl: sails.config.custom.uploadUrl,
      b2UploadUrl: res.data.uploadUrl,
      // apiUrl: res.data.apiUrl,
      sha1: inputs.sha1,
      fileName: sanitizedName,
      path: targetFolder.path,
      contentLength: file.contentLength,
      signature: encryptedJWT,
      // recommendedPartSize: res.data.recommendedPartSize,
      // absoluteMinimumPartSize: res.data.absoluteMinimumPartSize,
    };

    console.log(uploadParams);

    // All done.
    return uploadParams;

  }


};
