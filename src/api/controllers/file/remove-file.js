module.exports = {


  friendlyName: 'Remove file',


  description: '',


  inputs: {

    fileId: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    serverError: {
      responseType: 'serverError',
      description: 'Error communicating with B2 server.',
    },

    badRequest: {
      responseType: 'badRequest',
      description: 'File is already marked for removal.',
    }

  },


  fn: async function (inputs) {

    let file = await FileObject.findOne({
      where: {
        id: inputs.fileId,
        user: this.req.me.id,
      }
    })
    .populate('parentFolder');

    if (!file){ throw 'notFound'; }

    if (file.action === 'remove'){ return 'badRequest'; }

    let params = {
      userUuid: this.req.me.uuid,
      file: file,
    };

    await sails.helpers.removeFileByRecord.with(params)
    .intercept('serverError', 'serverError');

  }


};
