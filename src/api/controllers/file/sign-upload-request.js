module.exports = {


  friendlyName: 'Sign upload request',


  description: '',


  inputs: {

    fileName: {
      type: 'string',
      description: 'The file name of the file.',
      required: true,
      maxLength: 1024,
    },

    folderId: {
      type: 'string',
      required: true,
      maxLength: 100,
    },

    path: {
      type: 'string',
      required: true,
      maxLength: 1024,
    },

    contentType: {
      type: 'string',
      description: 'The MIME type of the content of the file.',
      maxLength: 64,
    },

    sha256: {
      type: 'string',
      description: 'SHA-256 checksums of the file',
      example: '2346ad27d7568ba9896f1b7da6b5991251debdf2',
      required: true,
      maxLength: 40,
    },

    contentLength: {
      type: 'number',
      min: 0,
    },

    fileInfo: {
      type: 'json',
      maxLength: 1024,
    }

  },


  exits: {

  },


  fn: async function (inputs) {

    var request = 'POST\n';
    request += 'https://' + sails.config.custom.s3Endpoint + '\n';
    request += 'acl=private\n';
    request += 'key=' + encodeURIComponent(String.trim(inputs.fileName)) + '\n';
    request += 'content-type:' + String.trim(inputs.contentType) + '\n';
    request += 'user-agent:parasails/0.9.2+axios/0.21.1\n';
    request += 'x-amz-algorithm';

  }


};
