module.exports = {


  friendlyName: 'Start large file',


  description: '',


  inputs: {

    fileName: {
      type: 'string',
      description: 'The file name of the file.',
      required: true,
      maxLength: 255,
    },

    folderId: {
      type: 'string',
      required: true,
      maxLength: 36,
      regex: /^[a-z0-9]+$/,
    },

    path: {
      type: 'string',
      required: true,
      maxLength: 1024,
    },

    contentType: {
      type: 'string',
      description: 'The MIME type of the content of the file.',
      maxLength: 64,
    },

    contentLength: {
      type: 'number',
      description: 'Size of the large file',
      isInteger: true,
      required: true,
      min: 1,
      max: 1000000000000,   // 1TB max per large file
    },

    sha1: {
      type: 'string',
      description: 'SHA-1 checksums of the file',
      example: '2346ad27d7568ba9896f1b7da6b5991251debdf2',
      minLength: 40,
      maxLength: 40,
      regex: /^[a-z0-9]+$/,
    },

    fileInfo: {
      type: 'string',
      description: 'Stringified JSON',
      maxLength: 512,
    },

  },


  exits: {

    serverError: {
      responseType: 'serverError',
      description: 'Error communicating with external server.',
    },

    folderNotFound: {
      responseType: 'notFound',
      description: 'Folder not found.',
    },

    badFileInfo: {
      responseType: 'badRequest',
      description: 'File info invalid.',
    },

    fileNameInvalid: {
      responseType: 'badRequest',
      description: 'File name invalid',
    },

    pathInvalid: {
      responseType: 'badRequest',
      description: 'Path invalid',
    },

    fileExists: {
      responseType: 'badRequest',
      description: 'File already exists.',
    },

    alreadyStarted: {
      responseType: 'badRequest',
      description: 'Large file upload already started.'
    }

  },


  fn: async function (inputs) {


    let checkFilename = await sails.helpers.checkFilename(inputs.fileName);
    if (!checkFilename){
      sails.log.debug('Filename: ' + inputs.fileName);
      throw 'fileNameInvalid';
    }

    let checkPath = await sails.helpers.checkPath(inputs.path);
    if (!checkPath){
      sails.log.debug('Path: ' + inputs.path);
      throw 'pathInvalid';
    }

    const sanitizedName = inputs.fileName;
    const decodedName = await sails.helpers.decodeFilename(sanitizedName);
    const sanitizedPath = inputs.path;

    // If fileInfo is not empty, check for syntax errors.
    let fileInfo = inputs.fileInfo;

    if (fileInfo)
    {
      try {
        fileInfo = JSON.parse(fileInfo);
      } catch(err){
        if (err){
          throw 'badFileInfo';
        }
      }
    }

    // Get current folder info
    let folder = await FolderObject.findOne({
      where: {
        id: inputs.folderId,
        user: this.req.me.id,
      }
    });

    if (!folder){
      throw 'notFound';
    }


    // TODO: check if any parent folder is marked for removal. If true, throw error.


    // Do all the required folders exist?
    let targetFolder = {};

    if (sanitizedPath === ''){
      targetFolder = folder;
    } else {

      // Search for folder directly

      targetFolder = await FolderObject.findOne({
        where: {
          user: this.req.me.id,
          path: sanitizedPath
        }
      });

      if (!targetFolder){

        let path = sanitizedPath.substring(folder.path.length-1, sanitizedPath.length);

        sails.log.debug(path);

        targetFolder = await sails.helpers.createFoldersRecursive.with({ userId: this.req.me.id, baseFolder: folder, path: path});

      } else {
        // sails.log.debug('Found folder by path.');
      }



    }

    sails.log.debug('Target folder path: ' + targetFolder.path );

    const decodedTargetPath = await sails.helpers.decodeFilename(targetFolder.path);

    // Is there an existing file with the same name?
    let fileSearch = await FileObject.findOne({
      where: {
        user: this.req.me.id,
        parentFolder: targetFolder.id,
        fileName: sanitizedName,
      }
    });

    if (fileSearch){
      if (fileSearch.action === 'upload'){
        throw 'fileExists';
      }

      if (fileSearch.action === 'start'){

        let uploadParams = {
          fileId: fileSearch.id,
          b2FileId: fileSearch.b2FileId,
          folderId: targetFolder.id,
          fileName: decodedName,
          path: decodedTargetPath,
          contentLength: fileSearch.contentLength,
          partSize: fileSearch.partSize,
          parts: fileSearch.parts,
        };

        return uploadParams;

        // throw 'alreadyStarted';

      }
    }


    // Start large file upload
    let axios = require('axios');
    let credentials = await sails.helpers.getAuthorizationToken();

    if (inputs.contentType === null || inputs.contentType === ''){
      inputs.contentType = 'b2/x-auto';
    }

    // Calculate number of parts
    let parts = 0;
    const size = inputs.contentLength;
    let partSize = credentials.recommendedPartSize;

    // console.log('Size: ' + size);
    // console.log('Parts: ' + parts);
    // console.log('Part size: ' + partSize);

    while (true){
      while (parts * partSize < size && parts < 10000){
        parts += 1;
      }

      // Increase partSize if exceeding max number of parts
      if (parts * partSize < size){
        partSize *= 2;
        parts = 0;
        continue;
      } else {

        console.log('Parts: '+parts);
        break;
      }
    }

    // console.log(fileInfo);
    // console.log(JSON.stringify(fileInfo));

    let reqParams = {
      'bucketId': sails.config.custom.bucketId,
      'fileName': sails.config.custom.b2FileNamePrefix + this.req.me.uuid + decodedTargetPath + decodedName,
      'contentType': inputs.contentType,
      'fileInfo': fileInfo,
    };

    let res = await axios({
      method: 'POST',
      url: credentials.apiUrl + '/b2api/v2/b2_start_large_file',
      headers: {
        'Authorization': credentials.authorizationToken,
        'User-Agent': sails.config.custom.apiUserAgent,
      },
      data: reqParams,

    }).catch( (err) => {
      if (err){
        sails.log.error(err);

        if (err.response){
          sails.log.error(err.response.data);
        }
      }
      throw 'serverError';
    });

    if (!res.data){
      console.log('Error getting upload URL. Abort.');
      throw 'serverError';
    }


    // Create file record
    let file = await FileObject.create({
      user: this.req.me.id,
      parentFolder: targetFolder.id,
      fileName: sanitizedName,
      contentType: inputs.contentType,
      contentLength: inputs.contentLength,
      fileInfo: inputs.fileInfo,
      action: 'start',
      b2FileId: res.data.fileId,
      partSize: credentials.recommendedPartSize,
      parts: parts,
    }).fetch();


    // Add file to folder
    await FolderObject.addToCollection(targetFolder.id, 'fileObjects').members(file.id);

    // Add to user file collection
    await User.addToCollection(this.req.me.id, 'fileObjects').members(file.id);


    let uploadParams = {
      fileId: file.id,
      b2FileId: res.data.fileId,
      folderId: targetFolder.id,
      fileName: decodedName,
      path: decodedTargetPath,
      contentLength: file.contentLength,
      partSize: partSize,
      parts: parts,
    };

    // console.log(uploadParams);

    return uploadParams;

    // All done.

  }


};
