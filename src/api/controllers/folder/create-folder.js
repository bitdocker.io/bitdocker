
module.exports = {


  friendlyName: 'Create folder',


  description: '',


  inputs: {

    folderName: {
      type: 'string',
      required: true,
      maxLength: 255,
    },

    parentFolderId: {
      type: 'string',
      required: true,
      maxLength: 36,
    },

  },


  exits: {

    badRequest: {
      responseType: 'badRequest',
      description: 'Folder name is invalid.',
    },

    notFound: {
      responseType: 'notFound',
      description: 'Parent folder not found.',
    },

  },


  fn: async function (inputs) {

    let checkFolderName = await sails.helpers.checkFilename(inputs.folderName);
    if (!checkFolderName){ throw 'badRequest'; }

    const sanitizedName = inputs.folderName;

    let params = {
      userId: this.req.me.id,
      folderName: sanitizedName,
      parentFolderId: inputs.parentFolderId,
    };

    let newFolder = await sails.helpers.createFolder.with(params);

    // All done.
    return newFolder;

  }


};
