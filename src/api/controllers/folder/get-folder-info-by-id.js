module.exports = {


  friendlyName: 'Get folder info',


  description: '',


  inputs: {

  },


  exits: {

    notFound: {
      responseType: 'notFound',
      description: 'Folder ID not found.'
    },

    serverError: {
      responseType: 'internalServerError',
      description: 'Error processing query.',
    }

  },


  fn: async function (inputs) {

    let folderId = this.req.query.folderId;

    if (!folderId){ throw 'notFound'; }

    let folder = await FolderObject.findOne({
      where:{
        id: folderId,
        user: this.req.me.id,
      }
    })
    .populate('fileObjects', {
      // sort: 'ASC fileName',
    })
    .populate('childFolders', {
      // sort: 'ASC folderName',
    });

    if (!folder){
      throw 'notFound';
    }

    // Resolve all parent folders

    let pathFolders = [];
    let parentFolderId = folder.parentFolder;

    while (parentFolderId && parentFolderId !== this.req.me.homeFolder){
      let grandFolder = await FolderObject.findOne({ id: parentFolderId});

      if (!grandFolder){ break; }

      let item = {
        folderName: grandFolder.folderName,
        folderId: grandFolder.id,
      };

      console.log(item);
      pathFolders.push(item);
      parentFolderId = grandFolder.parentFolder;

      // if (grandFolder.folderName === ':home'){ break; }
    }

    pathFolders.push({folderName: 'Home', folderId: this.req.me.homeFolder});

    // console.log(pathFolders);

    // console.log(folder);
    folder.pathFolders = pathFolders;

    return folder;

  }


};
