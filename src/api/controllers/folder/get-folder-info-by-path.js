module.exports = {


  friendlyName: 'Get folder info by path. NOT IMPLEMENTED.',


  description: '',


  inputs: {

  },


  exits: {

    notFound: {
      responseType: 'notFound',
      description: 'Folder ID not found.'
    },

    serverError: {
      responseType: 'internalServerError',
      description: 'Error processing query.',
    }

  },


  fn: async function (inputs) {

    let path = encodeURI(this.req.query.path);
    if (!path){ path = '/'; }

    let sort = this.req.query.sort;
    let order = this.req.query.order;
    let skip = this.req.query.skip;

    if (!sort){ sort = 'fileName'; }
    if (!order){ order = 'ASC'; }
    if (!skip){ skip = 0; }


    let userFiles = await User.findOne({ id: this.req.me.id }).populate('fileObjects', {
      where: {
        path: path
      },
      sort: sort + ' ' + order,
      skip: skip,
      limit: 100,
      select: ['id', 'contentLength', 'action', 'fileName', 'path', 'sha1', 'isLargeFile', 'parts', 'chunkSize', 'partsSha1', 'fileInfo', 'uploadTimestamp'],
    });

    let userFolders = await User.findOne({ 
      where: {
        id: this.req.me.id,
      },
      select: ['id', 'uuid'],
    })
    .populate('fileObjects', {
      where: {
        path: {
          '!=': path,
          'startsWith': path,
        }
      },
      sort: sort + ' ' + order,
      select: ['path'],
    });

    console.log(userFolders);

    let folderEntries = userFolders.fileObjects;
    let folders = {};

    console.log(path);
    console.log('^' + _.escapeRegExp(path) + '\/([^\/\:]+\/$)');

    for (let i=0; i < folderEntries.length; i++){
      let match = folderEntries[i].path.match(new RegExp('^' + _.escapeRegExp(path) + '([^\/\:]+\/$)'));
      // console.log(match);

      if (!match){
        // console.log('No match.');
        continue;
      } else {
        // console.log('Match.');
        if (!folders[folderEntries[i].path]){
          folders[folderEntries[i].path] = 1;
        } else {
          folders[folderEntries[i].path] += 1;
        }
      }

    }

    console.log(folders);
    console.log(JSON.stringify(folders));

    let files = userFiles.fileObjects;

    let res = {
      files: files,
      folders: JSON.stringify(folders),
    };

    console.log(res);

    // All done.
    return res;

  }


};
