module.exports = {


  friendlyName: 'Get folder size',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
      maxLength: 200,
    }

  },


  exits: {

    notFound: {
      responseType: 'notFound',
    }

  },


  fn: async function (inputs) {

    let {count,size} = await sails.helper.getFolderSize.with({id:inputs.id, user: this.req.me.id})
    .intecept( 'notFound', (err) => {
      if (err){
        throw 'notFound';
      }
    });

    // All done.
    return {count,size};

  }


};
