module.exports = {


  friendlyName: 'Remove folder',


  description: '',


  inputs: {

    folderId: {
      type: 'string',
      required: true,
      isNotEmptyString: true,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound',
      description: 'Folder not found.',
    },

  },


  fn: async function (inputs) {

    let folder = await FolderObject.findOne({
      where: {
        id: inputs.folderId,
        user: this.req.me.id,
      },
    })
    .populate('childFolders')
    .populate('fileObjects');

    if (!folder){ throw 'notFound'; }

    // Mark folder to be removed
    FolderObject.updateOne({
      where: {
        id: folder.id
      }
    }).set({
      action: 'remove',
    }).exec( (err) => {
      if (err){
        sails.log.error(err);
      }
    });

    let params = {
      userUuid: this.req.me.uuid,
      folder: folder,
    };

    await sails.helpers.removeFolderByRecord.with(params);

    // All done.
    return;

  }


};
