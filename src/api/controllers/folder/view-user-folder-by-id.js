
module.exports = {


  friendlyName: 'View user folder',


  description: 'Display "User folder" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/folder/user-folder'
    },

    notFound: {
      responseType: 'notFound',
      description: 'Folder not found.'
    },

    reload: {
      responseType: 'redirect'
    }

  },


  fn: async function () {

    const UID = this.req.me.id;

    if (!this.req.me.homeFolder){

      sails.log.debug('Initializing user folder.');
      let homeFolder = await sails.helpers.initializeUserFolder(UID);

      throw { 'reload': '/folder'};
    }

    return {};

  }


};
