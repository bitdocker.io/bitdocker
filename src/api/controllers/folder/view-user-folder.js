
module.exports = {


  friendlyName: 'View user folder',


  description: 'Display "User folder" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/folder/user-folder'
    },

    notFound: {
      responseType: 'notFound',
      description: 'Folder not found.'
    }

  },


  fn: async function () {

    let folderNameRegex = new RegExp('^\/(?:[^\:]+\/)?([^\:\/]+)\/$', 'g');

    let path = this.req.query.dir;
    if (!path){
      path = '/';
    }

    path = encodeURI(path);

    let folderName = ':home';
    let folderNameMatch = folderNameRegex.exec(this.req.query.dir);
    if (folderNameMatch){
      folderName = encodeURIComponent(folderNameMatch[1]);
    }

    let folder = null;

    // console.log(this.req.query.dir);
    // console.log('path:', path);
    // console.log('folderName:', folderName);

    if (path === '/' ){

      if (folderName === ':home'){
        console.log('Viewing home folder.');
        folder = await FolderObject.findOne({
          where: {
            id: this.req.me.homeFolder,
          }
        });

      } else {

        folder = await FolderObject.findOne({
          where: {
            parentFolder: this.req.me.homeFolder,
            user: this.req.me.id,
            folderName: folderName,
          }
        });

      }


    } else {

      if (folderName === ''){ throw 'notFound'; }

      folder = await FolderObject.find({
        where: {
          user: this.req.me.id,
          folderName: folderName,
          path: path,
        }
      })
      .sort('createdAt ASC');
    }

    if (folder.length < 1){
      throw 'notFound';
    } else {
      folder = folder[0];
    }

    return {
      path: folder.path,
      folderId: folder.id,
    };

  }


};
