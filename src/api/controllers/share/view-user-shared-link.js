module.exports = {


  friendlyName: 'View user shared link',


  description: 'Display "User shared link" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/share/user-shared-link'
    },

    notFound: {
      responseType: 'notFound',
    }

  },


  fn: async function () {

    // eslint-disable-next-line no-undef
    let shareLink = await ShareLink.findOne({
      where: {
        token: this.req.param('token'),
        expireAt: { '>': Date.now() },
      }
    })
    .populate('fileObjects')
    .populate('folderObjects');

    if (!shareLink){ throw 'notFound'; }
    else {

    }      



    // Respond with view.
    return {};

  }


};
