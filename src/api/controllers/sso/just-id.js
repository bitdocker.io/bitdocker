module.exports = {


  friendlyName: 'Just id',


  description: '',


  inputs: {

    returnUrl: {
      type: 'string',
      maxLength: 1000,
    },

    token: {
      type: 'string',
      maxLength: 1000,
    }

  },


  exits: {

    ssoRedirect: {
      responseType: 'ssoRedirect',
    },

    badCombo: {
      responseType: 'unauthorized',
      description: 'Email address or password not match. '
    },

    unauthorized: {
      responseType: 'unauthorized',
    },

    regoRedirect: {
      responseType: 'regoRedirect',
      description: 'User not setup here. Regirect to finish setup.'
    },

    redirect: {
      responseType: 'redirect',
    }

  },


  fn: async function ({returnUrl, token}) {

    if (!token){
      if (!returnUrl){ returnUrl = this.req.query.returnUrl; }
      if (!returnUrl){ returnUrl = sails.config.custom.baseUrl + '/sso/just-id'; }

      throw {'ssoRedirect': returnUrl};

    } else {
      var passport = require('passport');

      var req = this.req;
      var res = this.res;

      sails.log.debug('Just.ID login called.');

      let auth = await new Promise( (resolve) => {
        passport.authenticate('just-id', async (err, userRecord, expireAtSec, info) => {
          if((err) || (!userRecord)) {
            resolve(false);
            return false;
          }

          User.updateOne({
            id: userRecord.id,
          }).set({
            lastLoginIP: req.ip,
          }).exec( (err) => {
            if (err){
              sails.log.error('Error updating user login IP.');
            }
          });

          if (this.req.isSocket) {
            sails.log.warn(
              'Received `rememberMe: true` from a virtual request, but it was ignored\n'+
              'because a browser\'s session cookie cannot be reset over sockets.\n'+
              'Please use a traditional HTTP request instead.'
            );
          } else {
            this.req.session.cookie.maxAge = expireAtSec * 1000 - Date.now();
          }

          // Modify the active session instance.
          // (This will be persisted when the response is sent.)
          this.req.session.userId = userRecord.id;

          // In case there was an existing session (e.g. if we allow users to go to the login page
          // when they're already logged in), broadcast a message that we can display in other open tabs.
          if (sails.hooks.sockets) {
            await sails.helpers.broadcastSessionChange(this.req);
          }

          resolve(true);

        })(req, res);

      });

      sails.log.debug(auth);
      if (!auth){ throw 'unauthorized'; }

      throw {'redirect': '/'};
    }


  }


};
