module.exports = {


  friendlyName: 'Logout user',


  description: '',


  inputs: {

    jwt: {
      type: 'string',
      required: true,
      maxLength: 1000,
    }

  },


  exits: {

    unauthorized: {
      responseType: 'unauthorized',
    }

  },


  fn: async function ({jwt}) {

    // Decode jwt
    var jwtLib = require('jsonwebtoken');
    var decoded = {};

    try{
      decoded = jwtLib.verify(jwt, sails.config.custom.justIdPubKey);
    } catch(err){
      sails.log.error(err.message);
      sails.log.error('Could not decode JWT.');
      throw {'unauthorized': 'Invalid JWT'};
    }

    const userUuid = decoded.uuid;

    // Update DB in the background
    User.updateOne({ uuid: userUuid })
    .set({
      ssoLogout: true,
    })
    .exec((err) => {
      if (err){
        sails.log.error('Error marking user to be logged out by SSO!');
        sails.log.error(err.message);
      }
    });

  }


};
