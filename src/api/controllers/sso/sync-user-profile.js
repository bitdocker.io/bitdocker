module.exports = {


  friendlyName: 'Sync user profile',


  description: '',


  inputs: {

    jwt: {
      type: 'string',
      description: 'A JWT containing the user details.',
      required: true,
      maxLength: 4096,
    },

  },


  exits: {

    'badRequest': {
      responseType: 'badRequest',
    },

    'serverError': {
      responseType: 'serverError',
    },

    'notFound': {
      responseType: 'notFound',
      description: 'User not found.'
    }

  },


  fn: async function ({jwt}) {

    // Decode jwt
    var jwtLib = require('jsonwebtoken');
    var decoded = {};
    try{
      decoded = jwtLib.verify(jwt, sails.config.custom.justIdPubKey);
    } catch(err){
      sails.log.error(err.message);
      sails.log.error('Could not decode JWT.');
      throw {'badRequest': 'Could not decoded JWT'};
    }

    // Check parameters
    if (!decoded.uuid){
      throw {'badRequest': 'Missing user UUID'};
    }

    // Update fields
    // if (decoded.emailAddress && !decoded.emailStatus){ throw 'badRequest'; }

    let newRecords = {
      lastProfileUpdate: _.now(),
    };
    decoded.emailAddress? newRecords.emailAddress = decoded.emailAddress: null;
    decoded.emailStatus? newRecords.emailStatus = decoded.emailStatus: null;
    decoded.fullName? newRecords.fullName = decoded.fullName: null;

    let user = await User.updateOne({ uuid: decoded.uuid })
    .set(newRecords);

    if (!user){ throw 'notFound'; }

    // All done.
    return;


  }


};
