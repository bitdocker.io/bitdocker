module.exports = {


  friendlyName: 'View zip',


  description: 'Display "Zip" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/worker/zip'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
