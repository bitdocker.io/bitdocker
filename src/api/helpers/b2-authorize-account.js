module.exports = {


  friendlyName: 'B2 authorize account',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

    internalError: {
      description: 'Error communicating with B2 server.',
    },

  },


  fn: async function (inputs) {
    var axios = require('axios');

    var accountId = sails.config.custom.b2KeyId;
    var applicationKey = sails.config.custom.b2Key;
    var encodedKey = Buffer.from(accountId + ':' + applicationKey).toString('base64');

    var res = await axios({
      method: 'post',
      url: sails.config.custom.bucketApiUrl + '/b2api/v2/b2_authorize_account',
      headers: {
        'Authorization': 'Basic' + encodedKey,
        'User-Agent': sails.config.custom.apiUserAgent,
      },
      data: {},
    }).catch( (err) => {
      console.log(err);
    });

    if (!res.data){
      console.log('Error authorising B2 account. Abort.');
      throw 'internalError';
    }

    // console.log('Authorized account!');
    // console.log(res.data);

    await AuthToken.create({
      apiUrl: res.data.apiUrl,
      authorizationToken: res.data.authorizationToken,
      downloadUrl: res.data.downloadUrl,
      recommendedPartSize: res.data.recommendedPartSize,
    });


    var token = {
      apiUrl: res.data.apiUrl,
      authorizationToken: res.data.authorizationToken,
      downloadUrl: res.data.downloadUrl,
      recommendedPartSize: res.data.recommendedPartSize
    };

    return token;


  }


};

