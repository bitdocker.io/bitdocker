module.exports = {


  friendlyName: 'B 2 cancel large file',


  description: '',


  inputs: {

    file: {
      type: 'ref',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

    serverError: {
      description: 'Error talking to B2 server.',
    },

  },


  fn: async function ({file}) {

    let axios = require('axios');
    let credentials = await sails.helpers.getAuthorizationToken();

    let params = {
      fileId: file.b2FileId,
    };

    console.log(params);

    await axios({
      method: 'post',
      url: credentials.apiUrl + '/b2api/v1/b2_cancel_large_file',
      data: params,
      headers: {
        'Authorization': credentials.authorizationToken,
        'User-Agent': sails.config.custom.apiUserAgent,
      }
    })
    .then( (res) => {
      console.log(res.data);
      return res.data;
    })
    .catch( (err) => {
      console.log(err);

      if (err.response.data.code === 'file_not_present'){
        return;
      } else {
        throw 'serverError';
      }
    });

  }


};

