module.exports = {


  friendlyName: 'B2 delete file version',


  description: '',


  inputs: {

    userUuid: {
      type: 'string',
      required: true,
    },

    file: {
      type: 'ref',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

    serverError: {
      description: 'Error talking to B2 server.',
    },

  },


  fn: async function ({ userUuid, file }) {

    let axios = require('axios');
    let credentials = await sails.helpers.getAuthorizationToken();

    const encodedFilename = sails.config.custom.b2FileNamePrefix + userUuid + file.parentFolder.path + file.fileName;
    const decodedFilename = await sails.helpers.decodeFilename(encodedFilename);

    let params = {
      fileName: decodedFilename,
      fileId: file.b2FileId,
    };

    console.log(params);

    let res = await new Promise( (r) => {

      axios({
        method: 'post',
        url: credentials.apiUrl + '/b2api/v1/b2_delete_file_version',
        data: params,
        headers: {
          'Authorization': credentials.authorizationToken,
          'User-Agent': sails.config.custom.apiUserAgent,
        }
      })
      .then( (res) => {
        r(res.data);
      })
      .catch( (err) => {
        sails.log.error(err);

        if (err.response.data.code === 'file_not_present'){
          r(new Error('file_not_present'));
        } else {
          r(new Error('Server error'));
        }
      });

    });

    if (res instanceof Error){
      throw 'serverError';
    } else {
      return res;
    }


  }


};
