
module.exports = {


  friendlyName: 'B2 get download authorization',


  description: '',


  inputs: {

    fileId: {
      type: 'string',
      required: true,
    },

    userUuid: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

    serverError: {
      description: 'Error communicating with B2 Cloud.',
    }

  },


  fn: async function (inputs) {

    let file = await FileObject.findOne({ id: inputs.fileId })
    .populate('parentFolder');
    const fileNamePrefix = sails.config.custom.b2FileNamePrefix + inputs.userUuid + file.parentFolder.path + file.fileName;
    const decodedFileNamePrefix = await sails.helpers.decodeFilename(fileNamePrefix);

    console.log('Decoded: ' + decodedFileNamePrefix);

    let axios = require('axios');

    let credentials = await sails.helpers.getAuthorizationToken();

    let params = {
      bucketId: sails.config.custom.bucketId,
      fileNamePrefix: decodedFileNamePrefix,
      validDurationInSeconds: 900,   // 15 mins
    };



    let res =  await new Promise( async (resolve) => {
      await axios({
        method: 'POST',
        url: credentials.apiUrl + '/b2api/v1/b2_get_download_authorization',
        headers: {
          'Authorization': credentials.authorizationToken,
          'User-Agent': sails.config.custom.apiUserAgent,
        },
        data: params,
      })
      .then( (res) => {
        resolve(res.data);
      })
      .catch( (err) => {
        console.log(err);
        throw 'serverError';
      });
    });

    res.fileNamePrefix = fileNamePrefix;

    return res;

  }


};

