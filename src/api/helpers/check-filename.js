module.exports = {


  friendlyName: 'Check filename',


  description: '',


  inputs: {

    filename: {
      type: 'string',
      required: true,
      maxLength: 1000,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({filename}) {
    let sanitizedName = await sails.helpers.sanitizeFilename(filename);
    if (sanitizedName !== filename){ return false; }

    // Check for malformed URI encoding
    try{
      decodeURIComponent(sanitizedName);
    } catch(err){
      return false;
    }

    var regex = new RegExp(/[\/\\\#\&\+\,\?\s]/);
    var match = regex.exec(sanitizedName);
    if (match){
      return false;
    }

    // Decode-Encode Test. To find any UTF-16 encoded characters.
    var decoded = await sails.helpers.decodeFilename(sanitizedName);
    var encoded = await sails.helpers.encodeFilename(decoded);
    if (encoded !== sanitizedName){ return false; }

    return true;
  }


};

