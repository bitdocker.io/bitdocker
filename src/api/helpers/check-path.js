module.exports = {


  friendlyName: 'Check folder path',


  description: '',


  inputs: {

    path: {
      type: 'string',
      maxLength: 3000,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({path}) {

    if (!path || path?.length === 0){ return true; }

    let folderRegex = new RegExp('([^\/]+)', 'g');
    let folderNames = path.match(folderRegex);

    if (!folderNames){ return true; }

    for (let i=0; i < folderNames.length; i++){

      let folderName = folderNames[i];
      let checked = await sails.helpers.checkFilename(folderName);

      if (!checked){ return false; }
    }

    return true;

  }


};

