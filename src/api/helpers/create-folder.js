module.exports = {


  friendlyName: 'Create folder',


  description: '',


  inputs: {

    userId: {
      type: 'string',
      required: true,
    },

    parentFolderId: {
      type: 'string',
      required: true,
    },

    folderName: {
      type: 'string',
      description: 'Sanitized and fully encoded folder name.',
      required: true,
    },

    // path: {
    //   type: 'string',
    //   required: true,
    // }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    serverError: {
      description: 'Error processing database request.',
    },

    usageError: {
      description: 'Folder name invalid or folder already exist.'
    },

  },


  fn: async function (inputs) {

    // Find parent folder
    let parentFolder = null;

    parentFolder = await FolderObject.findOne({
      where: {
        id: inputs.parentFolderId,
        user: inputs.userId,
      }
    });

    if (!parentFolder){ throw 'notFound'; }

    // TODO: Ensure correct encoding of folder name
    const folderName = inputs.folderName;

    // Find any folder with the same name
    let existingFolder = await FolderObject.find({
      where: {
        user: inputs.userId,
        parentFolder: parentFolder.id,
        folderName: folderName,
      }
    })
    .sort('createdAt ASC');

    // TODO: merge folders if found more than one.

    if (existingFolder.length > 0){
      // console.log('Found existing folder: ' + existingFolder[0].path);
      return existingFolder[0];
    }

    // Create folder object
    let newFolder = await FolderObject.create({
      folderName: folderName,
      path: parentFolder.path + folderName + '/',
      user: inputs.userId,
      parentFolder: parentFolder.id,
    })
    .fetch();

    console.log('Created folder: ' + newFolder.path);

    // Add folder to parent folder
    await FolderObject.addToCollection(inputs.parentFolderId, 'childFolders').members([newFolder.id]);

    // Add folder to user
    await User.addToCollection(inputs.userId, 'folderObjects').members([newFolder.id]);

    return newFolder;
  }


};

