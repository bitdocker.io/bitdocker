module.exports = {


  friendlyName: 'Create folders recursive',


  description: '',


  inputs: {

    userId: {
      type: 'string',
      required: true,
      maxLength: 32,
    },

    baseFolder: {
      type: 'ref',
      description: 'base folder object',
      required: true,
    },

    path: {
      type: 'string',
      description: 'target folder path',
      required: true,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    badRequest: {
      description: 'Target folder path cannot be empty!'
    }

  },


  fn: async function ({ userId, baseFolder, path }) {

    if (path === '' || path === '/'){ return baseFolder; }

    let folderRegex = new RegExp('([^\/]+)', 'g');
    let folderNames = path.match(folderRegex);

    let parentFolder = baseFolder;
    let targetFolder = {};

    if (folderNames){
      for (let i=0; i < folderNames.length; i++){

        let folderName = folderNames[i];

        let folderSearch = await FolderObject.findOne({
          where: {
            user: userId,
            id: parentFolder.id,
            folderName: folderName,
          }
        });

        if (!folderSearch){
          let params = {
            userId: userId,
            parentFolderId: parentFolder.id,
            folderName: folderName,
          };

          let newFolder = await sails.helpers.createFolder.with(params);

          targetFolder = newFolder;
        } else {
          targetFolder = folderSearch;
        }

        parentFolder = targetFolder;

      }
    } // fi (folderNames)

    return targetFolder;

  }


};

