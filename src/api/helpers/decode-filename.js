module.exports = {


  friendlyName: 'Decode filename',


  description: '',


  inputs: {

    filename: {
      type: 'string',
      required: true,
      maxLength: 255,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({filename}) {

    let decoded = decodeURI(filename);

    decoded = decoded.replace(/\+/g, ' ');
    decoded = decoded.replace(/\%23/g, '#');
    decoded = decoded.replace(/\%24/g, '$');
    decoded = decoded.replace(/\%26/g, '&');
    decoded = decoded.replace(/\%2B/g, '+');
    decoded = decoded.replace(/\%2C/g, ',');
    decoded = decoded.replace(/\%3A/g, ':');
    decoded = decoded.replace(/\%3B/g, ';');
    decoded = decoded.replace(/\%3D/g, '=');
    decoded = decoded.replace(/\%3F/g, '?');
    decoded = decoded.replace(/\%40/g, '@');

    return decoded;

  }


};

