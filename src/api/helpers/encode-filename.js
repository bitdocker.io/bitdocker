const { encode } = require("punycode");

module.exports = {


  friendlyName: 'Encode filename',


  description: '',


  inputs: {

    filename: {
      type: 'string',
      required: true,
      maxLength: 255,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({filename}) {

    let encoded = encodeURI(filename);

    encoded = encoded.replace(/\#/g, '%23');
    encoded = encoded.replace(/\&/g, '%26');
    encoded = encoded.replace(/\+/g, '%2B');
    encoded = encoded.replace(/\,/g, '%2C');
    encoded = encoded.replace(/\?/g, '%3F');

    return encoded;

  }


};

