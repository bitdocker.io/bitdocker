module.exports = {


  friendlyName: 'Generate token',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },

  sync: true,


  fn: function (inputs) {
    const Crypto = require('crypto');

    function randomString(size) {
      return Crypto
        .randomBytes(size)
        .toString('base64')
        .slice(0, size);
    }

    return randomString(32);
  }


};

