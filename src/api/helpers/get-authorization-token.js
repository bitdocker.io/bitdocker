module.exports = {


  friendlyName: 'Get authorization token',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      outputFriendlyName: 'Authorization token',
    },

    badRequest: {
      description: 'Invalid request params.',
    }

  },


  fn: async function (inputs) {

    // Get authorization token.
    var credentials;

    var token = await AuthToken.find({
      where: {
        createdAt: { '>=': _.now() - 900000 },
      },
      limit: 1,
      sort: 'createdAt DESC'
    });

    // console.log(token);

    if (!token.length){
      credentials = await sails.helpers.b2AuthorizeAccount();
    } else {
      credentials = token[0];
    }

    // Send back the result through the success exit.
    return credentials;

  }


};

