module.exports = {


  friendlyName: 'Get aws signing key',


  description: '',


  inputs: {

    now: {
      type: 'number',
      required: true,
    },

    service: {
      type: 'string',
      required: true,
      example: 's3',
    },

  },


  exits: {

    success: {
      outputFriendlyName: 'Aws signing key',
    },

  },


  fn: async function ({now, service}) {
    const crypto = require('crypto');

    // Get aws signing key.

    const key = sails.config.custom.b2Key;
    const region = sails.config.custom.s3Region;

    const nowISO = new Date( now ).toISOString().replace(/[-\:]/g, '').substr(0,15) + 'Z';
    const nowDate = nowISO.substr(0,8);

    const dateKey = crypto.createHmac('sha256', key).update(nowDate).digest('hex');
    const dateRegionKey = crypto.createHmac('sha256', dateKey).update(region).digest('hex');
    const dateRegionServiceKey = crypto.createHmac('sha256', dateRegionKey).update(service).digest('hex');

    const signingKey = crypto.createHmac('sha256', dateRegionServiceKey).update('aws4_request').digest('hex');

    // Send back the result through the success exit.
    return signingKey;

  }


};

