module.exports = {


  friendlyName: 'Get folder size',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
      maxLength: 200,
    },

    user: {
      type: 'string',
      required: true,
      maxLength: 200,
    }

  },


  exits: {

    success: {
      outputFriendlyName: 'Folder size',
    },

    notFound: {
      description: 'Folder not found.',
    }

  },


  fn: async function ({id,user}) {

    let folder = await FolderObject.findOne({
      where: {
        id: id,
        user: user,
      }
    })
    .populate('fileObjects', {
      select: ['id', 'contentLength']
    });

    if (!folder){ throw 'notFound'; }

    let size = 0;
    let count = 0;

    if (folder.fileObjects){
      folder.fileObjects.forEach( (file) => {
        size += file.contentLength;
      });
    }

    if (folder.childFolders){
      folder.childFolders.forEach( async (subfolder) => {
        let {subCount, subSize} = await sails.helper.getFolderSize.with({ id: subfolder, user: user});
        count += subCount;
        size += subSize;
      });
    }


    return {
      count: count,
      size: size,
    };

  }


};

