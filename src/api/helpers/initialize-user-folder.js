module.exports = {


  friendlyName: 'Initialize user folder',


  description: '',


  inputs: {

    userId: {
      type: 'string',
      required: true,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

    notFound: {
      description: 'User not found',
    },

    badRequest: {
      description: 'User already has root folder.'
    },

    serverError: {
      description: 'Error creating root folder.'
    }

  },


  fn: async function ({userId}) {

    let user = await User.findOne({ id: userId });

    if (!user){ throw 'notFound'; }
    if (user.homeFolder){ throw 'badRequest'; }

    sails.log.debug('Creating user folder.');
    let homeFolder = await FolderObject.create({
      user: userId,
      folderName: ':home',
      path: '/',
    })
    .fetch();

    await User.updateOne({
      id: userId
    })
    .set({
      homeFolder: homeFolder.id,
    });

    return homeFolder;


  }


};

