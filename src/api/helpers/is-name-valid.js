module.exports = {


  friendlyName: 'Is name legal',


  description: '',


  inputs: {

    name: {
      type: 'string',
      required: true,
      maxLength: 256,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({name}) {
    if ( name.match(/[\:\?\/\<\>\|\*\"\\]/) ) {
      return false;
    } else {
      return true;
    }
  }


};

