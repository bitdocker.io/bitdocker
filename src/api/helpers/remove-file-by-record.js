module.exports = {


  friendlyName: 'Remove file by record',


  description: 'Given an user UUID and a populated file record, remove the file from B2 cloud and database.',


  inputs: {

    userUuid: {
      type: 'string',
      required: true,
    },

    file: {
      type: 'ref',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

    serverError: {
      description: 'Server Error',
    }

  },


  fn: async function ({userUuid, file}) {

    // If file was uploaded to B2, remove it from B2.
    if (file.action === 'upload'){
      let params = {
        userUuid: userUuid,
        file: file,
      };

      await FileObject.updateOne({ id: file.id})
      .set({ action: 'remove' });

      await sails.helpers.b2DeleteFileVersion.with(params)
      .intercept( 'serverError', 'serverError');

      // TODO: Check whether the file is already removed on B2, then remove local records.


    

    } else if (file.action === 'start'){
      // Remove all parts from DB
      await FileObjectPart.destroy({
        where: {
          fileObject: file.id
        }
      });

      // Cancel large file upload
      await sails.helpers.b2CancelLargeFile(file);
    }

    // Remove from folder
    FolderObject.removeFromCollection(file.parentFolder.id, 'fileObjects').members([file.id])
    .exec( (err) => {
      if (err) {
        sails.log.error(err);
      }
    });

    // Remove from user
    User.removeFromCollection(file.user, 'fileObjects').members([file.id])
    .exec( (err) => {
      if (err) {
        sails.log.error(err);
      }
    });

    // Remove file from database
    FileObject.destroyOne({ id: file.id })
    .exec( (err) => {
      if (err) {
        sails.log.error(err);
      }
    });

    // All done.
    return;
  }


};

