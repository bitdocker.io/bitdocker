module.exports = {


  friendlyName: 'Remove folder by record',


  description: 'Given an user UUID and a popuated folder record, remove all subfolders and files contained.',


  inputs: {

    userUuid: {
      type: 'string',
      required: true,
    },

    folder: {
      type: 'ref',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function ({userUuid, folder}) {

    async function removeFolder(folder){
      if (folder.childFolders){
        for (let i=0; i < folder.childFolders.length; i++){
          let subFolder = await FolderObject.findOne({ id: folder.childFolders[i].id })
          .populate('childFolders')
          .populate('fileObjects');

          await removeFolder(subFolder);
        }
      }

      if (folder.fileObjects){
        for (let j=0; j < folder.fileObjects.length; j++){
          let file = folder.fileObjects[j];
          file.parentFolder = folder;
          let params = {
            userUuid: userUuid,
            file: file,
          };
          await sails.helpers.removeFileByRecord.with(params);
        }
      }

      // Remove folder from user
      // if (folder.user !== null){
      //   await User.removeFromCollection(folder.user, 'folderObjects').members([folder.id]);
      // }
      // console.log('TICK!');

      // Remove folder from parent folder
      if (folder.parentFolder){
        await FolderObject.removeFromCollection(folder.parentFolder, 'childFolders').members([folder.id]);
      }
      // Remove folder from database
      await FolderObject.destroyOne({ id: folder.id });
    }

    await removeFolder(folder);

    return;
  }


};

