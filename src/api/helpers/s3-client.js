module.exports = {


  friendlyName: 'S3 client',


  description: 'Create a S3 client',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    // Load the SDK
    var AWS = require('aws-sdk');

    // Create an S3 client
    AWS.config.credentials = new AWS.Credentials( sails.config.custom.b2KeyId, sails.config.custom.b2Key);
    var ep = new AWS.Endpoint(sails.config.custom.s3Endpoint);
    var s3 = new AWS.S3({endpoint: ep});

    return s3;
  }


};

