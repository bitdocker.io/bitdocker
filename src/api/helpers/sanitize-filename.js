module.exports = {


  friendlyName: 'Sanitize filename',


  description: '',


  inputs: {

    filename: {
      type: 'string',
      required: true,
      maxLength: 255
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    var sanitize = require('sanitize-filename');

    return sanitize(inputs.filename);
  }


};
