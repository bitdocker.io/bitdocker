module.exports = {


  friendlyName: 'Update Cloudflare worker',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

    internalError: {
      description: 'Error communicating with B2 server.',
    },

  },


  fn: async function (inputs) {

    let cloudflareEmail = 'rickyipcw@protonmail.com';
    let bucketFilenamePrefix = sails.config.custom.b2FileNamePrefix;
    let cfZoneId = '4d4f2c97ffc2021abff2976300dc106c';
    let cfAppKey = 'db1ea8f57656c5bc5bba7b35d869ea57f1f5b';

    const maxSecondsAuthValid = 7*24*60*60; // one week in seconds


    // # DO NOT CHANGE ANYTHING BELOW THIS LINE ###

    let credentials = await sails.helpers.getAuthorizationToken();

    // # Get specific download authorization
    let axios = require('axios');

    let params = {
      bucketId: sails.config.custom.bucketId,
      fileNamePrefix: bucketFilenamePrefix,
      validDurationInSeconds: maxSecondsAuthValid,
    };

    let res =  await new Promise( async (resolve) => {
      await axios({
        method: 'POST',
        url: credentials.apiUrl + '/b2api/v1/b2_get_download_authorization',
        headers: {
          'Authorization': credentials.authorizationToken,
        },
        data: params,
      })
      .then( (res) => {
        resolve(res.data);
      })
      .catch( (err) => {
        console.log(err);
        throw 'serverError';
      });
    });

    let bDownAuToken = res.authorizationToken;

    let workerTemplate = `
addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
async function handleRequest(request) {
  let authToken='<B2_DOWNLOAD_TOKEN>'
  let b2Headers = new Headers(request.headers)
  b2Headers.append("Authorization", authToken)
  modRequest = new Request(request.url, {
      method: request.method,
      headers: b2Headers
  })
  const response = await fetch(modRequest)
  return response
}`;

    let workerCode = workerTemplate.replace('<B2_DOWNLOAD_TOKEN>', bDownAuToken)

    await axios({
      method: 'PUT',
      url: 'https://api.cloudflare.com/client/v4/zones/' + cfZoneId + '/workers/script',
      headers: {
        'X-Auth-Email' : cloudflareEmail,
        'X-Auth-Key' : cfAppKey,
        'Content-Type' : 'application/javascript',
      },
      data: workerCode
    })
    .then( (res) => {
      console.log(res.data);
    })
    .catch( (err) => {
      console.log(err);
      throw 'serveError';
    });

  }
};

