module.exports = {


  friendlyName: 'Uuid user',


  description: '',


  inputs: {

    string: {
      type: 'string',
      required: true,
      maxLength: 1000,
    }

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },

  sync: true,


  fn: function ({string}) {
    const { v5: uuidv5 } = require('uuid');

    return uuidv5(string, sails.config.custom.namespace.user);
  }


};

