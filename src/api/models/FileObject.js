/**
 * FileObject.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    // uuid: {
    //   type: 'string',
    //   required: true,
    //   unique: true,
    //   isUUID: true,
    // },

    fileName: {
      type: 'string',
      description: 'File name in percent encoded format.',
      required: true,
      isNotEmptyString: true,
      maxLength: 768,
    },

    contentType: {
      type: 'string',
      description: 'The MIME type of the content of the file.',
      defaultsTo: 'b2/x-auto',
      maxLength: 50,
    },

    contentLength: {
      type: 'number',
      description: 'Size of file in bytes.',
      required: true,
      isInteger: true,
      min: 0,
      max: 1000000000000,   // 1 TB
    },

    action: {
      type: 'string',
      description: 'The status of the file. start = Upload started. finish = Being finished. upload = Uploaded. remove = To be removed.',
      isIn: ['none', 'start', 'finish', 'upload',  'hide', 'folder', 'remove'],
      defaultsTo: 'none',
    },

    sha1: {
      type: 'string',
      description: 'Only for simple files: The SHA-1 checksum of the file.',
      example: '2346ad27d7568ba9896f1b7da6b5991251debdf2',
      maxLength: 40,
    },

    partsSha1: {
      type: 'json',
      description: 'Only for large files: An array of SHA-1 checksums for each part.',
    },

    fileInfo: {
      type: 'json',
      description: 'A JSON object holding the name/value pairs for the custom file info.',
    },

    b2FileId: {
      type: 'string',
      maxLength: 200,
    },

    uploadTimestamp: {
      type: 'number',
      description: 'This is a UTC time when this file was uploaded.',
      isInteger: true,
      example: 1502844074211,
      min: 0,
    },

    partSize: {
      type: 'number',
      description: 'The size of each part if the file was sliced.',
      isInteger: true,
      example: 100000000,
      min: 1,
      defaultsTo: 100000000,
    },

    parts: {
      type: 'number',
      description: 'How many parts is the file divided into for uploading.',
      isInteger: true,
      required: true,
      min: 1,
      max: 10000,
    },

    downloadCounter: {
      type: 'number',
      description: 'Count how many times the file has been downloaded.',
      isInteger: true,
      defaultsTo: 0,
      min: 0,
    },

    shareAllowed: {
      type: 'boolean',
      description: 'Can the file can be shared.',
      defaultsTo: true,
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    user: {
      model: 'user',
      description: 'Associated user.',
    },

    parentFolder: {
      model: 'folderobject',
      description: 'Parent folder.',
    },

    fileObjectParts: {
      collection: 'FileObjectPart',
      via: 'fileObject',
      description: 'Associated file parts.',
    },

    shareLinks: {
      collection: 'shareLink',
      via: 'fileObjects',
      description: 'Associated share link(s).',
    },

  },

};

