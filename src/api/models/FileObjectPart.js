/**
 * FileObjectPart.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    action: {
      type: 'string',
      isIn: ['start', 'upload'],
      defaultsTo: 'start',
    },

    contentLength: {
      type: 'number',
      required: true,
      isInteger: true,
      example: 1200000000,
      min: 0,
    },

    sha1: {
      type: 'string',
      required: true,
      example: '2346ad27d7568ba9896f1b7da6b5991251debdf2',
      maxLength: 40,
    },

    uploadTimestamp: {
      type: 'number',
      description: 'This is a UTC time when this file was uploaded.',
      isInteger: true,
      example: 1502844074211,
      min: 0,
    },

    partNumber: {
      type: 'number',
      isInteger: true,
      required: true,
      min: 1,
      max: 10000,
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    fileObject: {
      model: 'FileObject',
    },


  },

};

