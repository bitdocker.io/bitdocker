/**
 * FolderObject.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    folderName: {
      type: 'string',
      required: true,
      maxLength: 256,
    },

    path: {
      type: 'string',
      required: true,
      maxLength: 1024,
    },

    folderFileSize: {
      type: 'number',
      defaultsTo: 0,
      isInteger: true,
      example: 120000000,
      min: 0,
    },

    folderStatLastUpdated: {
      type: 'number',
      isInteger: true,
      example: 1502844074211,
      min: 0,
    },

    folderFileCount: {
      type: 'number',
      isInteger: true,
      example: 12345,
      defaultsTo: 0,
      min: 0,
    },

    action: {
      type: 'string',
      isIn: ['upload', 'remove'],
      defaultsTo: 'upload',
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    user: {
      model: 'User',
      required: true,
    },

    parentFolder: {
      model: 'folderObject',
    },

    childFolders: {
      collection: 'folderObject',
      via: 'parentFolder',
    },

    fileObjects: {
      collection: 'fileObject',
      via: 'parentFolder',
    },

    shareLinks: {
      collection: 'shareLink',
      via: 'folderObjects',
    },

  },

};

