/**
 * ShareLink.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    disabled: {
      type: 'boolean',
      defaultsTo: false,
    },

    token: {
      type: 'string',
      description: 'An unique ID to access the share.',
      unique: true,
      required: true,
      maxLength: 100,
    },

    doesNotExpire: {
      type: 'boolean',
      defaultsTo: true,
    },

    expireAt: {
      type: 'number',
      isInteger: true,
      defaultsTo: 0,
      example: 1502844074211,
      min: 0,
    },

    protected: {
      type: 'boolean',
      defaultsTo: false,
    },

    password: {
      type: 'string',
      description: 'A SHA-1 hash of the password.',
      protect: true,
      defaultsTo: '',
      maxLength: 40,
    },

    comment: {
      type: 'string',
      description: 'A user supplied comment.',
      maxLength: 200,
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    user: {
      model: 'User',
    },

    fileObjects: {
      collection: 'fileObject',
      via: 'shareLinks',
    },

    folderObjects: {
      collection: 'folderObject',
      via: 'shareLinks',
    },



  },

};

