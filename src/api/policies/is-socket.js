/**
 * is-socket
 *
 * A simple policy that rejects non-socket requests
 *
 */
 module.exports = async function (req, res, proceed) {

  // If the request if not from a socket, return badRequest.
  if (!req.isSocket) {
    return res.badRequest();
  }

  // Proceed otherwise
  return proceed();

};
