/**
 * ssoRedirect.js
 *
 * A custom response.
 *
 * Example usage:
 * ```
 *     return res.ssoRedirect();
 *     // -or-
 *     return res.ssoRedirect(optionalData);
 * ```
 *
 * Or with actions2:
 * ```
 *     exits: {
 *       somethingHappened: {
 *         responseType: 'ssoRedirect'
 *       }
 *     }
 * ```
 *
 * ```
 *     throw 'somethingHappened';
 *     // -or-
 *     throw { somethingHappened: optionalData }
 * ```
 */

module.exports = function ssoRedirect(optionalData) {
  // expected data: returnUrl

  // Get access to `req` and `res`
  var req = this.req;
  var res = this.res;

  // Define the status code to send in the response.
  var statusCodeToSet = 302;

  if (optionalData === undefined) {
    sails.log.info('Ran custom response: res.ssoRedirect()');
    // res.status(302);
    return res.redirect( sails.config.custom.ssoBaseUrl + '/sso/login?returnUrl=' + encodeURI(sails.config.custom.baseUrl));

  }
  // Else if the provided data is an Error instance, if it has
  // a toJSON() function, then always run it and use it as the
  // response body to send.  Otherwise, send down its `.stack`,
  // except in production use res.sendStatus().
  else if (_.isError(optionalData)) {
    sails.log.info('Custom response `res.regoRedirect()` called with an Error:', optionalData);

    // If the error doesn't have a custom .toJSON(), use its `stack` instead--
    // otherwise res.json() would turn it into an empty dictionary.
    // (If this is production, don't send a response body at all.)
    if (!_.isFunction(optionalData.toJSON)) {
      if (process.env.NODE_ENV === 'production') {
        return res.sendStatus(statusCodeToSet);
      }
      else {
        return res.status(statusCodeToSet).send(optionalData.stack);
      }
    }
  } else {

    try{
      encodeURI(optionalData);
    } catch(err){
      // If returnUrl is not a valid URL, set returnURL to baseURL
      return res.redirect( sails.config.custom.ssoBaseUrl + '/sso/login?returnUrl=' + encodeURI(sails.config.custom.baseUrl));
    }

    console.log('SSO redirect param:', encodeURI(optionalData));
    // res.status(302);
    return res.redirect( sails.config.custom.ssoBaseUrl + '/sso/login?returnUrl=' + encodeURI(optionalData) );

  }

};
