/**
 * cloud.setup.js
 *
 * Configuration for this Sails app's generated browser SDK ("Cloud").
 *
 * Above all, the purpose of this file is to provide endpoint definitions,
 * each of which corresponds with one particular route+action on the server.
 *
 * > This file was automatically generated.
 * > (To regenerate, run `sails run rebuild-cloud-sdk`)
 */

Cloud.setup({

  /* eslint-disable */
  methods: {"confirmEmail":{"verb":"GET","url":"/email/confirm","args":["token"]},"grantCsrfToken":{"verb":"GET","url":"/csrf"},"logout":{"verb":"GET","url":"/api/v1/account/logout","args":[]},"updatePassword":{"verb":"PUT","url":"/api/v1/account/update-password","args":["password"]},"updateProfile":{"verb":"PUT","url":"/api/v1/account/update-profile","args":["fullName","emailAddress"]},"updateBillingCard":{"verb":"PUT","url":"/api/v1/account/update-billing-card","args":["stripeToken","billingCardLast4","billingCardBrand","billingCardExpMonth","billingCardExpYear"]},"login":{"verb":"PUT","url":"/api/v1/entrance/login","args":["emailAddress","password","rememberMe"]},"signup":{"verb":"POST","url":"/api/v1/entrance/signup","args":["emailAddress","password","fullName"]},"sendPasswordRecoveryEmail":{"verb":"POST","url":"/api/v1/entrance/send-password-recovery-email","args":["emailAddress"]},"updatePasswordAndLogin":{"verb":"POST","url":"/api/v1/entrance/update-password-and-login","args":["password","token"]},"deliverContactFormMessage":{"verb":"POST","url":"/api/v1/deliver-contact-form-message","args":["emailAddress","topic","fullName","message"]},"observeMySession":{"verb":"POST","url":"/api/v1/observe-my-session","args":[],"protocol":"io.socket"},"getDownloadUrl":{"verb":"GET","url":"/api/v1/file/get-download-url","args":[]},"getDownloadParams":{"verb":"GET","url":"/api/v1/file/get-download-params","args":[]},"getUploadUrl":{"verb":"POST","url":"/api/v1/file/get-upload-url","args":["fileName","folderId","path","contentType","sha1","contentLength"]},"completeFileUpload":{"verb":"POST","url":"/api/v1/file/complete-file-upload","args":["fileId","b2FileId","sha1","fileInfo"]},"removeFile":{"verb":"PUT","url":"/api/v1/file/remove-file","args":["fileId"]},"getFolderInfoById":{"verb":"GET","url":"/api/v1/folder/get-folder-info-by-id","args":[]},"removeFolder":{"verb":"PUT","url":"/api/v1/folder/remove-folder","args":["folderId"]},"updateFolder":{"verb":"PUT","url":"/api/v1/folder/update-folder","args":[]},"createFolder":{"verb":"POST","url":"/api/v1/folder/create-folder","args":["folderName","parentFolderId"]},"justId":{"verb":"GET","url":"/sso/just-id","args":["returnUrl","token"]}}
  /* eslint-enable */

});
