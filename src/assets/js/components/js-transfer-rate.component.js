/**
 * <js-transfer-rate>
 * -----------------------------------------------------------------------------
 * A human-readable, self-updating "timeago" timestamp, with some special rules:
 *
 * • Within 24 hours, displays in "timeago" format.
 * • Within a month, displays month, day, and time of day.
 * • Within a year, displays just the month and day.
 * • Older/newer than that, displays the month and day with the full year.
 *
 * @type {Component}
 * -----------------------------------------------------------------------------
 */

parasails.registerComponent('jsTransferRate', {

  //  ╔═╗╦═╗╔═╗╔═╗╔═╗
  //  ╠═╝╠╦╝║ ║╠═╝╚═╗
  //  ╩  ╩╚═╚═╝╩  ╚═╝
  props: [
    'rate',
    'round',
  ],

  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: function (){
    return {
      formattedRate: '',
    };
  },

  //  ╦ ╦╔╦╗╔╦╗╦
  //  ╠═╣ ║ ║║║║
  //  ╩ ╩ ╩ ╩ ╩╩═╝
  template: `
  <span>{{formattedRate}}</span>
  `,

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    if (this.rate === undefined || this.rate < 0) {
      throw new Error('Incomplete usage of <js-transfer-rate>:  Please specify `rate` as a non-negative number.');
    }

    this._formatRate();

  },

  beforeDestroy: function() {

  },

  watch: {
    rate: function() {
      this._formatRate();
    }
  },


  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    _formatRate: function() {
      let step = 0;
      let rate = this.rate;
      let unit = '';

      while ( rate / 1000 >= 1 && step <= 5){
        step += 1;
        rate /= 1000;
      }

      switch(step){
        case 0:
          unit = 'B/s';
          break;
        case 1:
          unit = 'KB/s';
          break;
        case 2:
          unit = 'MB/s';
          break;
        case 3:
          unit = 'GB/s';
          break;
        case 4:
          unit = 'TB/s';
          break;
        default:
          unit = 'PB/s';
          break;
      }

      let round = 0;
      if (this.round){
        rount = this.round;
      }

      if (rate < 1000000 || round === 0){
        this.formattedRate = Math.round(rate) + ' ' + unit;
      } else {
        this.formattedRate = Math.round(rate*Math.pow(10,round))/Math.pow(10,round) + ' ' + unit;
      }
    }

  }

});
