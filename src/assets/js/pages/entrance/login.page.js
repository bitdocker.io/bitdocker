parasails.registerPage('login', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    returnUrl: '',
    countdown: 3,
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {

  },
  mounted: async function() {
    if (this.sso && this.redirectUrl){
      window.location = this.redirectUrl;
    }

    this.autoRedirect();
  },

  computed: {

    loadingDots: function() {
      let dots = '';
      for (let i=6; i > this.countdown; i--){
        dots += '.';
      }

      return dots;
    }
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    async useJustIdLogin(){
      this.goto('/sso/just-id' + (this.returnUrl? '?returnUrl=' + this.returnUrl : ''));
    },

    async autoRedirect(){

      while(true){
        await new Promise( r => setTimeout(r, 1000));
        this.countdown -= 1;
        if (this.countdown === 0){ break; }
      }

      // console.log('Redirect');
      await this.useJustIdLogin();
    }

  }
});
