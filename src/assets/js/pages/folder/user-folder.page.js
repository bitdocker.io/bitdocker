parasails.registerPage('user-folder', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    _csrf: '',
    _csrfTimestamp: 0,

    modal: '',

    // Form data
    formError: {},
    cloudError: '',
    formData: {},

    loading: true,
    notFound: false,

    sort: 'name',
    order: 'ASC',
    // skip: 0,
    // loadedAll: false,

    uploadOverlay: false,

    path: '/',
    pathFolders: [],
    folderId: '',
    dirData: {},
    folders: [],
    files: [],

    folderSelection: [],
    fileSelection: [],

    // Queues
    requestQueue: [],
    uploadQueue: [],

    // Progress monitors
    scanningFiles: false,
    processingRequestQueue: false,
    uploading: false,

    uploadThreads: 0,
    uploadThreadsLimit: 4,

    uploadSize: 0,
    uploadProgressSize: 0,
    requestSize: 0,

    uploadCount: 0,
    requestCount: 0,

    uploadRate: 0,
    uploadRates: {},
    uploadedSizes: {},
    lastRefreshedUploadRate: 0,

    downloadThreads: 0,
    downloadThreadsLimit: 4,

    threadID: 0,

    // Removing files
    removing: false,
    removeCount: 0,
    removeTotal: 0,

    // uploadPercentage: 0,
    // downloadPercentage: 0,

    apiUserAgent: 'parasails/0.9.2+axios/0.21.1',
    b2FileNamePrefix: 'user/',

    // // Feature flags
    // useBlobFallback: false,
    
    // Worker - Zip
    // zipFrames: {},
    // zipFrameReady: {},
    // messageChannels: {},

  },

  watch: {

  },

  computed: {
    selectAll: {
      get: function () {
        if (!this.folders.length && !this.files.length){ return false; }
        let totalCount = 0;
        let totalSelected = this.folderSelection.length + this.fileSelection.length;
        this.folders? totalCount += this.folders.length: null;
        this.files? totalCount += this.files.length: null;
        return  totalSelected === totalCount? true: false;
      },

      set: function (v) {
        let folderSelected = [];
        let fileSelected = [];

        if (v) {
          this.folders.forEach( (folder) => {
            folderSelected.push(folder.id);
          });
          this.files.forEach( (file) => {
            fileSelected.push(file.id);
          });
        }

        this.folderSelection = folderSelected;
        this.fileSelection = fileSelected;
      }
    }
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    //…
  },
  mounted: async function() {

    io.sails.autoConnect = true;

    io.socket.on('error', async () => {
      console.log('Cannot reconnect to server. Trying again...');
    });

    io.socket.on('disconnect', async () => {
      console.log('Socket disconnected!');

      while (!io.socket.isConnected()){
        await new Promise((r) => { setTimeout(r, 2000); });
        if (!io.socket.isConnecting()){
          try{
            await io.socket.reconnect();
          } catch (err){
            console.log('Error reconnecting.');
          }
        }
      }

      if (io.socket.isConnected()){
        console.log('WebSocket connection established.');
      }
    });


    // Test browser capabilities
    await this.testCapabilities();

  },


  //  ╦  ╦╦╦═╗╔╦╗╦ ╦╔═╗╦    ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ╚╗╔╝║╠╦╝ ║ ║ ║╠═╣║    ╠═╝╠═╣║ ╦║╣ ╚═╗
  //   ╚╝ ╩╩╚═ ╩ ╚═╝╩ ╩╩═╝  ╩  ╩ ╩╚═╝╚═╝╚═╝
  // Configure deep-linking (aka client-side routing)
  virtualPagesRegExp: /^\/folder\/?([^\/]+)?\/?/,
  afterNavigate: async function(virtualPageSlug){
    // `virtualPageSlug` is determined by the regular expression above, which
    // corresponds with `:unused?` in the server-side route for this page.
    // switch (virtualPageSlug) {
    //   case 'hello':
    //     this.modal = 'example';
    //     break;
    //   default:
    //     this.modal = '';
    // }

    if (virtualPageSlug){
      this.folderId = virtualPageSlug;
      // console.log('Navigated to: ' + virtualPageSlug);
    } else {
      this.folderId = this.me.homeFolder;
      // console.log('Root folder.');
    }

    await this.refreshFolder(true);

  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {


    async testCapabilities(){
      this.useBlobFallback = /constructor/i.test(window.HTMLElement) || !!window.safari || !!window.WebKitPoint;
    },


    createZipFrame(){
      // Generate a local downloadID
      const downloadID = this.getRandomID();

      // Create an invisible iframe using the downloadID
      let zipFrame = document.createElement('iframe');
      zipFrame.style = 'display: none; ';
      zipFrame.name = downloadID;
      zipFrame.src = '/worker/zip';
      document.body.appendChild(zipFrame);

      // Wait for the iframe to load
      zipFrame.addEventListener("load", this.onZipFrameLoad(downloadID));

      this.zipFrames[downloadId] = zipFrame;

      return downloadID;
    },

    // Once zipFrame is loaded
    onZipFrameLoad(downloadID){
      // Set up MessageChannel
      const mc = new MessageChannel();
      this.messageChannels[downloadID] = mc;

      // Listen for messages on port1
      mc.port1.onmessage = this.onZipFrameMessage;
      // Transfer port2 to the iframe
      this.zipFrames[downloadID].contentWindow.postMessage('hello', '*', [mc.port2]);
    },

    // Handle messages received on port1
    onZipFrameMessage(e){
      console.log('[Port 1]:', e);

      if (e.type === 'message' && e.data === 'state:ready'){
        // Done preparing service worker in iframe
        // this.workerZipReady = true;
      }
    },

    // Check whether a zip frame is ready to accept stream
    async checkZipFrameState(downloadID){
      const mc = this.messageChannels[downloadID];

      let readyState = await new Promise( r => {
        mc.port1.onmessage = (e) => {
          if (!e.check || e.check.localeCompare(downloadID) !== 0){
            this.zipFrameReady[downloadID] = false;
            r(false);
          }

          this.zipFrameReady[downloadID] = true;
          r(true);
        };

        this.zipFrames[downloadID].contentWindow.postMessage({check: downloadID}, '*');
      });

      mc.port1.onmessage = this.onZipFrameMessage;
      return readyState;
    },


    // ██╗   ██╗██████╗ ██╗      ██████╗  █████╗ ██████╗
    // ██║   ██║██╔══██╗██║     ██╔═══██╗██╔══██╗██╔══██╗
    // ██║   ██║██████╔╝██║     ██║   ██║███████║██║  ██║
    // ██║   ██║██╔═══╝ ██║     ██║   ██║██╔══██║██║  ██║
    // ╚██████╔╝██║     ███████╗╚██████╔╝██║  ██║██████╔╝
    //  ╚═════╝ ╚═╝     ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝


    async uploadFile(item, params, fileData){

      let fileName = this.b2FileNamePrefix + this.me.uuid + params.path + params.fileName;
      let sha1 = params.sha1;
      let contentType = item.file.type;
      let lastModified = item.file.lastModified;

      let lastLoaded = 0;
      let lastProgressTime = 0;

      // console.log(fileName);

      let res = await new Promise( (resolve) => {
        // eslint-disable-next-line no-undef
        axios({
          method: 'POST',
          url: params.uploadUrl,
          data: fileData,
          headers: {
            'Content-Type': contentType? contentType: 'b2/x-auto',
            'User-Agent': this.apiUserAgent,
            'X-Bz-Content-Sha1': sha1,
            'X-Bz-File-Name': fileName,
            'X-Bz-Info-src_last_modified_millis': lastModified,
            // 'X-Bz-Test-Mode': 'fail_some_uploads',
            'x-io-signature': params.signature,
            'x-io-uploadurl': params.b2UploadUrl,
          },
          onUploadProgress: (e) => {

            if (lastProgressTime === 0){
              lastLoaded = e.loaded/e.total;
              lastProgressTime = Date.now();
            } else if (Date.now() - lastProgressTime >= 1000){
              let pc = e.loaded/e.total;
              let progressRate = pc - lastLoaded;
              let progressSize = progressRate * params.contentLength;

              // Save rate as Byte/Second
              let uploadRate = Math.round(progressSize / ((Date.now() - lastProgressTime) / 1000));
              this.uploadRates[params.requestID] = uploadRate;
              this.uploadedSizes[params.requestID] = pc * params.contentLength;

              this.refreshUploadRate();

              lastLoaded = pc;
              lastProgressTime = Date.now();
            }
          }
        })
        .then( async (res) => {
          // console.log(res.data);
          // console.log('Uploaded to B2.');

          delete this.uploadRates[params.requestID];
          delete this.uploadedSizes[params.requestID];
          this.refreshUploadRate(true);

          let ackData = {
            fileId: params.fileId,
            b2FileId: res.data.fileId,
            sha1: params.sha1,
            fileInfo: JSON.stringify(res.data.fileInfo),
            contentType: res.data.contentType,
            _csrf: await this.getCSRF(),
          };

          // Report back to server
          await new Promise( async (r) => {
            io.socket.post('/api/v1/file/finish-file-upload', ackData, (res, jwres) => {
              if (jwres.statusCode < 400){
                r(0);
              } else {
                console.log('Error finishing file upload!');
                console.log(jwres);
                r(-1);
              }
            });
          });


          resolve(0);


        })
        .catch( (err) => {
          if (err){
            delete this.uploadRates[params.requestID];
            delete this.uploadedSizes[params.requestID];
            this.refreshUploadRate(true);

            if (err.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(err.response.data);
              console.log(err.response.status);
              // console.log(err.response.headers);
            } else if (err.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(err.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', err.message);
            }
            // console.log(params);
          }

          // Requeue file
          resolve(-1);
        });
      });

      return res;



    },


    async uploadPart(item, params, fileData, partNumber){

      let sha1 = params.sha1;
      // let contentLength = tail - head;
      // let lastModified = item.file.lastModified;
      let authToken = params.authorizationToken;
      // let parts = params.parts;

      let lastLoaded = 0;
      let lastProgressTime = 0;

      let res = new Promise( (resolve) => {
        axios({
          method: 'post',
          url: params.uploadUrl,
          data: fileData,
          headers: {
            'Authorization': authToken,
            'User-Agent': this.apiUserAgent,
            'X-Bz-Content-Sha1': sha1,
            'X-Bz-Part-Number': partNumber,
          },
          onUploadProgress: (e) => {

            if (lastProgressTime === 0){
              lastLoaded = e.loaded/e.total;
              lastProgressTime = Date.now();
            } else if (Date.now() - lastProgressTime >= 1000){
              let pc = e.loaded/e.total;
              let progressRate = pc - lastLoaded;
              let progressSize = progressRate * params.contentLength;

              // Save rate as Byte/Second
              let uploadRate = Math.round(progressSize / ((Date.now() - lastProgressTime) / 1000));
              this.uploadRates[params.requestID] = uploadRate;
              this.uploadedSizes[params.requestID] = pc * params.contentLength;

              this.refreshUploadRate();

              lastLoaded = e.loaded/e.total;
              lastProgressTime = Date.now();
            }
          }
        })
        .then( async (res) => {
          // console.log(res.data);
          // console.log('Uploaded to B2.');

          delete this.uploadRates[params.requestID];
          delete this.uploadedSizes[params.requestID];
          this.refreshUploadRate(true);

          let ackData = {
            fileId: params.fileId,
            sha1: params.sha1,
            partNumber: partNumber,
            _csrf: await this.getCSRF(),
          };

          let retry = 0;
          let acknowledged = false;

          while (!acknowledged && retry < 5){

            if (retry > 0){
              await new Promise( (r) => { setTimeout(r(), 3000); } );
            }

            let ackRes = await new Promise( (r) => {
              io.socket.post('/api/v1/file/finish-file-part-upload', ackData, async (res, jwres) => {
                if (jwres.statusCode < 400){

                  // If all parts are uploaded, try to finish large file.
                  if ( res.upload === params.parts) {

                    let ackData = {
                      fileId: params.fileId,
                      _csrf: await this.getCSRF(),
                    };

                    let retry = 0;
                    let acknowledged = false;

                    while (!acknowledged && retry < 5){

                      if (retry > 0){
                        await new Promise( (r) => { setTimeout(r(), 3000); } );
                      }

                      let finishRes = await new Promise( async (r) => {
                        io.socket.post('/api/v1/file/finish-large-file-upload', ackData, (res, jwres) => {
                          if (jwres.statusCode < 400){
                            r(0);
                          } else {
                            console.log('Error finishing file upload!');
                            console.log(jwres);
                            r(-1);
                          }
                        });
                      });

                      if (finishRes === 0){
                        acknowledged = true;
                      }

                      retry += 1;

                    }

                    // Large file upload complete
                    if (acknowledged){
                      this.uploadCount++;
                    } else {
                      console.log('Error finishing large file upload: ' + params.fileName);
                    }

                  } // fi res.upload === params.parts

                  r(res.upload);

                } else {
                  console.log('Error reporting part upload!');
                  console.log(jwres);
                  r(-1);
                }
              });
            });

            if (ackRes){
              acknowledged = true;
            }

            retry += 1;

          }

          // Upload part completed
          if (acknowledged){
            resolve(0);
          } else {
            resolve(-2);
          }

        })
        .catch( (err) => {
          if (err){
            delete this.uploadRates[params.requestID];
            delete this.uploadedSizes[params.requestID];
            this.refreshUploadRate(true);

            if (err.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(err.response.data);
              console.log(err.response.status);
              // console.log(err.response.headers);
            } else if (err.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(err.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log('Error', err.message);
            }
            // console.log(params);
          }

          resolve(-1);
        });

      });


      return res;

    },



    async processUploadQueue(){
      while (this.uploadThreads >= this.uploadThreadsLimit){
        await new Promise((r) => { setTimeout(r, 1000); });
      }

      this.uploadThreads += 1;

      let entry = await this.uploadQueue.shift();
      if (!entry){
        this.uploadThreads -= 1;
        return;
      }

      let item = entry.item;
      let uploadParams = entry.uploadParams;

      // console.log(uploadParams);

      if (!entry.isPart){
        // Simple file upload
        let fileData = await this.getFileData(item.file);

        console.log('Uploading: ' + uploadParams.path + uploadParams.fileName);

        let  res = await this.uploadFile(item, uploadParams, fileData);

        if (res < 0){
          console.log(res);
          console.log('Simple upload failed. Requeuing...');

          this.uploadThreads -= 1;

          await this.requestQueue.push(item);
          this.processRequestQueue();

          return;
        }

        console.log('Uploaded: ' + uploadParams.path + uploadParams.fileName);
        this.uploadCount += 1;
        this.uploadSize += item.file.size;

      } else {
        // File part upload
        let partNumber = entry.partNumber;
        let head = entry.head;
        let tail = entry.tail;
        let fileData = await this.getFilePartData(item.file, head, tail);

        console.log('Uploading part (' + entry.partNumber + '): ' + uploadParams.path + uploadParams.fileName);

        let res = await this.uploadPart(item, uploadParams, fileData, partNumber, head, tail);

        if (res < 0){
          console.log(res);
          console.log('Upload part failed. Requeuing...');

          this.uploadThreads -= 1;

          await this.queueFilePart(item, uploadParams, partNumber);
          this.processRequestQueue();

          return;
        }

        // Upload part completed
        this.uploadSize += (tail - head);

      }

      this.uploadThreads -= 1;

      if (this.uploadQueue.length > 0 || this.requestQueue.length > 0 || this.uploadThreads > 0){
        await this.processUploadQueue();
      } else {

        // Reset upload stats
        this.resetUploadStats();
        await this.refreshFolder(true);
      }
    },

    async requestSimpleUploadParams(item){
      // Get file upload url
      let _csrf = await this.getCSRF();

      // Calculate file checksum
      let sha1 = await this.getFileSha1(item.file);

      let data = {
        _csrf: _csrf,
        fileName: item.fileName,
        folderId: this.folderId,
        path: item.path,
        contentType: item.file.type,
        contentLength: item.file.size,
        sha1: sha1,
      };

      let uploadParams = await new Promise( async (resolve) => {
        // Get upload parameters
        await io.socket.post('/api/v1/file/get-upload-url', data, async (res, jwres) => {
          if (jwres.statusCode < 400){
            // console.log(res);
            resolve(res);

          } else {
            console.log('Error requesting upload URL! (' + jwres.statusCode + ')');
            console.log(jwres);
            resolve(null);
          }
        });

      });

      if (!uploadParams){ return null; }

      return uploadParams;
    },


    async requestLargeFileUploadParams(item){
      // Get large file upload url
      let _csrf = await this.getCSRF();

      // Calculate file checksum
      // let sha1 = await this.getFileSha1(item.file);

      // console.log(sha1);

      let data = {
        _csrf: _csrf,
        fileName: item.fileName,
        folderId: this.folderId,
        path: item.path,
        contentType: item.file.type,
        contentLength: item.file.size,
        // sha1: sha1,
        fileInfo: JSON.stringify({
          'src_last_modified_millis': item.file.lastModified.toString()
        })
      };

      let uploadParams = await new Promise( async (resolve) => {
        // Get upload parameters
        await io.socket.post('/api/v1/file/start-large-file', data, async (res, jwres) => {
          if (jwres.statusCode < 400){
            // console.log(res);
            resolve(res);

          } else {
            console.log('Error requesting upload URL! (' + jwres.statusCode + ')');
            console.log(jwres);
            resolve();
          }
        });

      });

      return uploadParams;
    },


    async requestPartUploadParams(item, uploadParams, partNumber, head, tail){
      // Get file upload url
      let _csrf = await this.getCSRF();

      console.log('Processing part', partNumber, 'of', item.fileName, '(', head, ',', tail, ')');

      // Calculate file checksum
      let sha1 = await this.getFilePartSha1(item.file, head, tail);

      let data = {
        _csrf: _csrf,
        fileId: uploadParams.fileId,
        contentLength: tail - head,
        sha1: sha1,
        partNumber: partNumber,
      };

      let params = await new Promise( async (resolve) => {
        // Get upload parameters
        await io.socket.post('/api/v1/file/get-upload-part-url', data, async (res, jwres) => {
          if (jwres.statusCode < 400){
            // console.log(res);
            resolve(res);

          } else {

            if (jwres.headers['X-Exit'] !== 'partExists'){
              console.log('Error requesting upload URL! (' + jwres.statusCode + ')');
              console.log(jwres);
            }

            resolve(jwres);
          }
        });

      });

      if (params.statusCode && params.statusCode >= 400){

        let exit = params.headers['X-Exit'];

        switch (exit){
          case 'partExists':
            console.log('[Notice] Part ' + partNumber + ' of ' + uploadParams.fileName + ' is already uploaded.');
            return params;

          default:
            return null;
        }

      }

      let uploadPartParams = _.extend(params, uploadParams);

      uploadPartParams.sha1 = sha1;
      uploadPartParams.contentLength = tail - head;

      console.log('Upload part params:', uploadPartParams);

      return uploadPartParams;
    },



    // ██████╗ ███████╗ ██████╗ ██╗   ██╗███████╗███████╗████████╗
    // ██╔══██╗██╔════╝██╔═══██╗██║   ██║██╔════╝██╔════╝╚══██╔══╝
    // ██████╔╝█████╗  ██║   ██║██║   ██║█████╗  ███████╗   ██║
    // ██╔══██╗██╔══╝  ██║▄▄ ██║██║   ██║██╔══╝  ╚════██║   ██║
    // ██║  ██║███████╗╚██████╔╝╚██████╔╝███████╗███████║   ██║
    // ╚═╝  ╚═╝╚══════╝ ╚══▀▀═╝  ╚═════╝ ╚══════╝╚══════╝   ╚═╝


    async processRequestQueue(){
      while ( this.processingRequestQueue || this.uploadQueue.length > 4){
        await new Promise((r) => { setTimeout(r, 1000); });
      }

      this.processingRequestQueue = true;

      let item = await this.requestQueue.shift();
      if (!item){
        this.processingRequestQueue = false;
        return;
      }

      // console.log('Processing:', item);

      if (item.file.size <= 100000000){
        // If file size <= 100 MB
        let uploadParams = await this.requestSimpleUploadParams(item);
        if (!uploadParams){
          // Failed to get upload params. Cancel request.
          this.processingRequestQueue = false;

          if (this.uploadQueue.length <= 0 && this.requestQueue.length <= 0 && this.uploadThreads <= 0){
            // Reset upload stats
            this.resetUploadStats();
            await this.refreshFolder(true);
          }

          return;
        }

        uploadParams.requestID = this.getRandomID();

        await this.uploadQueue.push({item: item, uploadParams: uploadParams, isPart: false});
        console.log('Queued file for upload: ' + this.decodeFileName(uploadParams.fileName));

        this.processUploadQueue();

      } else {

        // Large files > 100 MB
        console.log('Large File Upload.');

        // Start large file upload
        let uploadParams = await this.requestLargeFileUploadParams(item);

        if (!uploadParams){
          console.log('[Error] No response from requesting large file upload params.');

          this.processingRequestQueue = false;

          if (this.uploadQueue.length <= 0 && this.requestQueue.length <= 0 && this.uploadThreads <= 0){
            // Reset upload stats
            this.resetUploadStats();
            await this.refreshFolder(true);
          }

          return;
        }

        // Process parts and get upload part URLs

        for (let i=0; i < uploadParams.parts; i++){
          let queueRes = await this.queueFilePart(item, uploadParams, i+1);
          if (queueRes === 0){ this.processUploadQueue(); }
          await new Promise((r) => { setTimeout(r, 300); });
        }

      }

      this.processingRequestQueue = false;

      this.processRequestQueue();
      this.processUploadQueue();
    },

    async queueFilePart(item, uploadParams, partNumber){
      let partSize = uploadParams.partSize;
      let head = partSize * (partNumber-1);
      let tail = head + partSize;

      if (tail > item.file.size){ tail = item.file.size; }

      let uploadPartParams = await this.requestPartUploadParams(item, uploadParams, partNumber, head, tail);

      if (!uploadPartParams){
        console.log('[Error] Failed to request params for part ' + partNumber + ' of ' + uploadParams.fileName);
        return -1;
      }

      // Part already exists. Skip uploading this part.
      if (uploadPartParams.statusCode && uploadPartParams.statusCode >= 400 && uploadPartParams.headers['X-Exit'] === 'partExists'){
        // Count upload part completed
        this.uploadSize += (tail - head);

        return -2;
      }

      uploadPartParams.requestID = this.getRandomID();

      await this.uploadQueue.push({item, uploadParams: uploadPartParams, isPart: true, partNumber, head, tail});

      console.log('Queued part (' + uploadParams.partNumber + ') for upload: ' + this.decodeFileName(uploadParams.fileName));

      return 0;
    },


    // Process DataTransfer objects into file blobs and resolve their paths
    async processDataTransferItem(item, path){

      if (item.isDirectory) {
        let directoryReader = await item.createReader();
        let newPath = path + this.encodeFileName(this.sanitizeFileName(item.name)) + '/';

        await directoryReader.readEntries( async (entries) => {
          await entries.forEach( async (entry) => {
            await this.processDataTransferItem(entry, newPath);
          });
        });

      } else if (item.isFile){

        await item.file( async (file)=>{
          console.log('Pushing file to request queue.');
          await this.requestQueue.push({
            file: file,
            fileName: this.encodeFileName(this.sanitizeFileName(item.name)),
            path: path,
          });

          this.uploading = true;
          this.requestCount++;
          this.requestSize += file.size;

          // Signal process request queue
          this.processRequestQueue();
        });

      } else if (item instanceof File){

        console.log('Pushing file to request queue.');
        await this.requestQueue.push({
          file: item,
          fileName: this.encodeFileName(this.sanitizeFileName(item.name)),
          path: path,
        });

        this.uploading = true;
        this.requestCount++;
        this.requestSize += item.size;

        // Signal process request queue
        this.processRequestQueue();

      }

    },


    // Parse deag-and-drop file entries into DataTransfer objects
    async scanFiles(e){
      if (this.scanningFiles){
        return;
      } else {
        this.scanningFiles = true;
        this.uploadOverlay = false;
      }

      console.log('Scanning files');

      let itemList = e.dataTransfer.items;
      // A DataTransferItemList object containing DataTransferItem objects representing the items being dragged in a drag operation.

      if (!itemList || !itemList.length){
        console.log('No file selected!');
        return;
      }

      for (let i=0; i < itemList.length; i++){
        let item = itemList[i];

        // This function is implemented as webkitGetAsEntry() in non-WebKit browsers including Firefox at this time; it may be renamed to getAsEntry() in the future. Looking for both.
        let entry = null;
        try{
          entry = await item.webkitGetAsEntry();
        } catch (err){
          console.log(err);

          try{
            entry = await item.getAsEntry();
          } catch (err){
            console.log(err);
            console.log('Error getting file entry. Please check browser compatibility.');
          }

        }

        if (!entry){ continue;}
        // If the item described by the DataTransferItem is a file, webkitGetAsEntry() returns a FileSystemFileEntry or FileSystemDirectoryEntry representing it. If the item isn't a file, null is returned.

        await this.processDataTransferItem( entry, this.path);
      }

      // Done scanning for files
      this.scanningFiles = false;

    },


    async showFilePicker(){

      const filePicker = document.getElementById('filePicker');

      filePicker.addEventListener('input', async (e) => {
        // Process file list
        const fileList = filePicker.files;
        console.log(fileList);

        for (let i=0; i < fileList.length; i++){
          const entry = fileList[i];
          await this.processDataTransferItem(entry, this.path);
        }
      })

      filePicker.click();

    },








    // ██████╗  ██████╗ ██╗    ██╗███╗   ██╗██╗      ██████╗  █████╗ ██████╗
    // ██╔══██╗██╔═══██╗██║    ██║████╗  ██║██║     ██╔═══██╗██╔══██╗██╔══██╗
    // ██║  ██║██║   ██║██║ █╗ ██║██╔██╗ ██║██║     ██║   ██║███████║██║  ██║
    // ██║  ██║██║   ██║██║███╗██║██║╚██╗██║██║     ██║   ██║██╔══██║██║  ██║
    // ██████╔╝╚██████╔╝╚███╔███╔╝██║ ╚████║███████╗╚██████╔╝██║  ██║██████╔╝
    // ╚═════╝  ╚═════╝  ╚══╝╚══╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝


    // Fetch a downloadUrl from BitDocker for a single file using fileId
    async getDownloadUrl(fileId){

      let res = await new Promise( async (resolve) => {
        io.socket.get('/api/v1/file/get-download-params?fileId='+fileId, (res, jwres) => {
          if (jwres.statusCode < 400){
            // console.log('Got download params:');
            console.log(res);
            resolve(res);
          } else {
            console.log(jwres);
          }
        });
      });

      let downloadUrl = res.downloadUrl + '?Authorization=' + res.authorizationToken;

      return downloadUrl;

    },

    // Trigger the downloading of a single file
    async downloadFile(fileId, fileName){

      // Get downloadUrl from BitDocker
      let downloadUrl = await this.getDownloadUrl(fileId);

      // Generate a local downloadID
      const downloadID = this.getRandomID();

      // Create an invisible iframe using the downloadID
      let downloadFrame = document.createElement('iframe');
      downloadFrame.style = 'display: none; ';
      downloadFrame.setAttribute('name', downloadID);
      document.body.appendChild(downloadFrame);

      // Create an invisible button with the downloadUrl
      let downloadBtn = document.createElement('a');
      downloadBtn.href = downloadUrl;
      downloadBtn.setAttribute('download', fileName);
      downloadBtn.setAttribute('target', downloadID);
      document.body.appendChild(downloadBtn);

      // Click the button and then remove it
      downloadBtn.click();
      downloadBtn.remove();

      // Remove the iframe after 30 sec
      await new Promise( (r) => {setTimeout(r, 30000);} );
      downloadFrame.remove();

    },

    async testDownload(){

    },

    urlToPromise(url) {
      return new Promise((resolve, reject) => {
        // eslint-disable-next-line no-undef
        JSZipUtils.getBinaryContent(url, (err, data) => {
          if(err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      });
    },

    // Trigger the downloading of all selected files and then package them into a zip file locally
    async downloadSelected() {
      // Draft feature
      // Files -> Zip -> Stream -> Browser

      return;

      // // Get file and folder selection
      // const files = this.fileSelection;
      // const folders = this.folderSelection;
      // // let currentPath = this.path;
      // let totalSize = 0;
      // let totalCount = 0;

      // // Create a new JSZip object
      // var zipFile = new JSZip();

      // // Creat a write stream for downloading
      // // const fileStream = streamSaver.createWriteStream('download.zip');

      // // Loop through all selections
      // for (file in files){

      //   // Find fileId and its corresponding filename
      //   const fileId = files[file];
      //   let fileName = null;
      //   for (let i=0; i < this.files.length; i++){
      //     if (this.files[i].id === fileId){
      //       console.log(this.files[i]);
      //       fileName = this.decodeFileName(this.files[i].fileName);
      //       totalSize += this.files[i].contentLength;
      //       break;
      //     }
      //   }

      //   if (totalSize / Math.pow(1024,2) > 500){
      //     console.log('Total file size cannot be larger than 500MB.');
      //     delete zipFile;
      //     return;
      //   }

      //   console.log('Download selected: ' + files[file] + ' (' + fileName + ')');

      //   // Fetch downloadUrl for a single file
      //   const downloadUrl = await this.getDownloadUrl(fileId);

      //   // Append file promise to zipFile
      //   await zipFile.file(fileName, this.urlToPromise(downloadUrl));
      // }


      // // Download generated zip file and pipe data to file stream
      // zipFile.generateAsync({type:"blob", stream: true}).then((blob) => { // 1) generate the zip file
      //   // const readableStream = blob.stream();

      //   // TODO: Download zip file stream
      //   // saveAs(blob, 'download.zip');

      // }, (err) => {
      //   console.log(err);
      // });

    },



    // ███████╗ ██████╗ ██╗     ██████╗ ███████╗██████╗
    // ██╔════╝██╔═══██╗██║     ██╔══██╗██╔════╝██╔══██╗
    // █████╗  ██║   ██║██║     ██║  ██║█████╗  ██████╔╝
    // ██╔══╝  ██║   ██║██║     ██║  ██║██╔══╝  ██╔══██╗
    // ██║     ╚██████╔╝███████╗██████╔╝███████╗██║  ██║
    // ╚═╝      ╚═════╝ ╚══════╝╚═════╝ ╚══════╝╚═╝  ╚═╝


    async goToFolderById(folderId){
      // if (!io.socket.isConnected()){ return; }

      this.clearSelection();
      if (folderId === this.folderId){
        this.refreshFolder(true);
      } else {
        this.goto('/folder/'+folderId);
      }
    },

    async getDirData(){

      let query = '?folderId=' + this.folderId;

      let res = new Promise( r => {
        io.socket.get('/api/v1/folder/get-folder-info-by-id' + query, (res, jwres) => {
          if (jwres.statusCode < 400){
  
            this.dirData = res;
            this.path = res.path;

            // Update page title
            if (res.id === this.me.homeFolder){
              document.title = 'Your files';
            } else {
              document.title = this.decodeFileName(this.dirData.folderName);
            }
  
            this.notFound = false;
            console.log('Path: ' + this.path + '; folderId: ' + this.folderId );
  
            r();
          } else {
            console.log('Error getting folder info.');
            console.log(jwres);
            this.notFound = true;
            r();
          }
        });
      });

      return res;

    },


    async refreshFolder(fetchDirData){
      this.clearSelection();
      
      if (fetchDirData){
        this.loading = true;
        await this.getDirData();
      }

      // Sort folder
      if (this.sort === 'name'){
        if (this.order === 'ASC'){
          this.folders = this.dirData.childFolders.sort((a,b) => (a.folderName > b.folderName)? 1:-1);
          this.files = this.dirData.fileObjects.sort((a,b) => (a.fileName > b.fileName)? 1:-1);
        } else {
          this.folders = this.dirData.childFolders.sort((a,b) => (a.folderName > b.folderName)? -1:1);
          this.files = this.dirData.fileObjects.sort((a,b) => (a.folderName > b.folderName)? -1:1);
        }

      } else if (this.sort === 'size'){
        if (this.order === 'ASC'){
          // Folder size is not calculated by default
          // this.folders = this.dirData.childFolders.sort((a,b) => (a.contentLength > b.contentLength)? 1:-1);
          this.files = this.dirData.fileObjects.sort((a,b) => (a.contentLength > b.contentLength)? 1:-1);
        } else {
          this.files = this.dirData.fileObjects.sort((a,b) => (a.contentLength > b.contentLength)? -1:1);
        }

      } else if (this.sort === 'createdAt'){
        if (this.order === 'ASC'){
          this.folders = this.dirData.childFolders.sort((a,b) => (a.createdAt > b.createdAt)? 1:-1);
          this.files = this.dirData.fileObjects.sort((a,b) => (a.createdAt > b.createdAt)? 1:-1);
        } else {
          this.folders = this.dirData.childFolders.sort((a,b) => (a.createdAt > b.createdAt)? -1:1);
          this.files = this.dirData.fileObjects.sort((a,b) => (a.createdAt > b.createdAt)? -1:1);
        }

      } else {
        this.folders = this.dirData.childFolders.sort((a,b) => (a.folderName > b.folderName)? 1:-1);
        this.files = this.dirData.fileObjects.sort((a,b) => (a.fileName > b.fileName)? 1:-1);
      }

      // Set folder path breadcrumb
      this.pathFolders = [];

      for (let i=0; i < this.dirData.pathFolders.length; i++){
        this.pathFolders.push(this.dirData.pathFolders[this.dirData.pathFolders.length - 1 - i]);
      }

      // Finished loading folder
      this.loading = false;
    },

    async removeFolder(folderId){
      if (!io.socket.isConnected()){ return; }

      let params = {
        folderId: folderId,
        _csrf: await this.getCSRF(),
      };

      io.socket.put('/api/v1/folder/remove-folder', params, (res, jwres) => {
        if (jwres.statusCode < 400){

        } else {
          console.log(jwres);
        }
      });

      for (let i=0; i < this.folders.length; i++) {
        if (this.folders[i].id === folderId){
          this.folders[i].action = 'remove';
          break;
        }
      }

    },

    async removeOneFolder(folderId) {
      if (!io.socket.isConnected()){ return; }

      await this.removeFolder(folderId);
      await this.refreshFolder(true);
    },

    async createFolder(parentFolerId, folderName){
      if (!io.socket.isConnected()){ return; }

      // Check folder name
      if (!folderName || folderName === '' || folderName.length > 1024){
        this.formError.folderName = true;
        return;
      }

      if (!this.isNameValid(folderName)){
        this.formError.folderName = true;
        return;
      }

      let _csrf = await this.getCSRF();

      await new Promise( async (resolve) => {
        let params = {
          parentFolderId: parentFolerId,
          folderName: this.encodeFileName(folderName),
          _csrf: _csrf,
        };

        io.socket.post('/api/v1/folder/create-folder', params, (res, jwres) => {
          if (jwres.statusCode < 400){
            resolve(res);

            this.refreshFolder(true);
            this.closeModal();
          } else {
            console.log('Error creating folder.');
            console.log(jwres);

            this.cloudError = res;
            resolve(jwres.statusCode);
          }
        });

      });


    },











    // ███████╗██╗██╗     ███████╗
    // ██╔════╝██║██║     ██╔════╝
    // █████╗  ██║██║     █████╗
    // ██╔══╝  ██║██║     ██╔══╝
    // ██║     ██║███████╗███████╗
    // ╚═╝     ╚═╝╚══════╝╚══════╝


    async removeFile(fileId){
      if (!io.socket.isConnected()){ return; }

      let params = {
        fileId: fileId,
        _csrf: await this.getCSRF(),
      };

      // console.log('Sending remove file request.');
      // console.log(params);

      await new Promise( (resolve) => {
        io.socket.put('/api/v1/file/remove-file', params, (res, jwres) => {
          if (jwres.statusCode < 400){
            resolve(res);
          } else {
            console.log(jwres);
          }
        });
      });

    },

    async removeOneFile(fileId) {
      if (!io.socket.isConnected()){ return; }

      await this.removeFile(fileId);
      await this.refreshFolder(true);
    },


    async removeSelected() {
      if (!io.socket.isConnected()){ return; }

      let files = this.fileSelection;
      let folders = this.folderSelection;

      if (!files || !folders){ return; }
      if (!files.length && !folders.length){ return; }

      for (let i=0; i < files.length; i++) {
        await this.removeFile(files[i]);
        console.log('Removed file', files[i]);
      }

      for (let i=0; i < folders.length; i++) {
        await this.removeFolder(folders[i]);
        console.log('Removed folder', folders[i]);
      }

      this.folderSelection = [];
      this.fileSelection = [];

      await this.refreshFolder(true);
    },








    /////////////////////////////////
    // Sharing functions
    /////////////////////////////////



    async createShareLink(selectedItems, comment){


    },







    // ██╗   ██╗██╗
    // ██║   ██║██║
    // ██║   ██║██║
    // ██║   ██║██║
    // ╚██████╔╝██║
    //  ╚═════╝ ╚═╝


    async sortByName(){
      if (this.sort !== 'name' || this.order === 'DESC'){
        this.order = 'ASC';
      } else {
        this.order = 'DESC';
      }
      this.sort = 'name';
      await this.refreshFolder();
    },

    async sortBySize(){
      if (this.sort !== 'size' || this.order === 'DESC'){
        this.order = 'ASC';
      } else {
        this.order = 'DESC';
      }
      this.sort = 'size';
      await this.refreshFolder();
    },

    async sortByCreatedAt(){
      if (this.sort !== 'createdAt' || this.order === 'DESC'){
        this.order = 'ASC';
      } else {
        this.order = 'DESC';
      }
      this.sort = 'createdAt';
      await this.refreshFolder();
    },







    // ██╗  ██╗███████╗██╗     ██████╗ ███████╗██████╗ ███████╗
    // ██║  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔════╝
    // ███████║█████╗  ██║     ██████╔╝█████╗  ██████╔╝███████╗
    // ██╔══██║██╔══╝  ██║     ██╔═══╝ ██╔══╝  ██╔══██╗╚════██║
    // ██║  ██║███████╗███████╗██║     ███████╗██║  ██║███████║
    // ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝╚══════╝

    async getCSRF(){

      if (this._csrf !== '' && this._csrfTimestamp > _.now() - 1800000){
        // console.log('Using existing CSRF token.');
        return this._csrf;
      } else {
        // eslint-disable-next-line no-undef
        let res = await axios.get('/csrf')
        .catch( (err) => {
          if (err.response){
            console.log(err.response.data);
            console.log(err.response.status);
          } else {
            console.log(err.message);
            console.log('Error obtaining CSRF token.');
          }

          return '';
        });
        this._csrf = res.data._csrf;
        this._csrfTimestamp = _.now();

        return res.data._csrf;
      }

    },

    async getFileSha1(file){
      let sha1 = await new Promise( async (resolve) => {
        const reader = new FileReader();
        reader.onloadend = ()=>{
          // Extract raw data and prase it into word array for hashing.
          let wordArray = CryptoJS.lib.WordArray.create(reader.result);

          const checksum = CryptoJS.SHA1(wordArray).toString();
          resolve(checksum);
        };
        reader.readAsArrayBuffer(file);
      });
      return sha1;
    },

    async getFilePartSha1(file, head, tail){
      let sha1 = await new Promise( async (resolve) => {
        const reader = new FileReader();
        reader.onloadend = ()=>{
          // Extract raw data and prase it into word array for hashing.
          let wordArray = CryptoJS.lib.WordArray.create(reader.result);

          const checksum = CryptoJS.SHA1(wordArray).toString();
          resolve(checksum);
        };
        let slice = file.slice(head, tail);
        reader.readAsArrayBuffer(slice);
      });
      // console.log('SHA1 (' + head + ',' + tail + '): ' + sha1);
      return sha1;
    },

    async getFileData(file){
      let fileData = await new Promise( async (resolve) => {
        const reader = new FileReader();
        reader.onloadend = ()=>{
          // Return raw file data
          resolve(reader.result);
        };
        reader.readAsArrayBuffer(file);
      });
      return fileData;
    },

    async getFilePartData(file, head, tail){
      let fileData = await new Promise( async (resolve) => {
        const reader = new FileReader();
        reader.onloadend = ()=>{
          // Return raw file data
          resolve(reader.result);
        };
        let slice = file.slice(head, tail);
        reader.readAsArrayBuffer(slice);
      });
      return fileData;
    },

    isNameValid(name){
      if (!name || name.length < 1){ return false; }
      if (name.length > 256){ return false; }

      if (name.match(/[\:\?\/\<\>\|\*\"\\]/)){ return false; }

      return true;
    },

    // isNameValid(name){
    //   if (!name || name.length < 1){ return false; }
    //   if (name.length > 256){ return false; }

    //   let regex = new RegExp('[\:]');
    //   if (regex.exec(name)){ return false; }

    //   return true;
    // },

    sanitizeFileName(name){
      let newName = name.replace(/[\:\?\/\<\>\|\*\"\\]/, '_');
      return newName;
    },

    encodeFileName(name){
      let encoded = encodeURI(name);

      encoded = encoded.replace(/\#/g, '%23');
      encoded = encoded.replace(/\&/g, '%26');
      encoded = encoded.replace(/\+/g, '%2B');
      encoded = encoded.replace(/\,/g, '%2C');
      encoded = encoded.replace(/\?/g, '%3F');

      return encoded;
    },

    decodeFileName(name){
      let decoded = decodeURI(name);

      decoded = decoded.replace(/\+/g, ' ');
      decoded = decoded.replace(/\%23/g, '#');
      decoded = decoded.replace(/\%24/g, '$');
      decoded = decoded.replace(/\%26/g, '&');
      decoded = decoded.replace(/\%2B/g, '+');
      decoded = decoded.replace(/\%2C/g, ',');
      decoded = decoded.replace(/\%3A/g, ':');
      decoded = decoded.replace(/\%3B/g, ';');
      decoded = decoded.replace(/\%3D/g, '=');
      decoded = decoded.replace(/\%3F/g, '?');
      decoded = decoded.replace(/\%40/g, '@');

      return decoded;
    },

    getRandomID(prefix){
      prefix? null: prefix = '';
      return prefix + '_' + _.now() + Math.floor(10000*Math.random());
    },

    async refreshUploadRate(force){
      if (!force && Date.now() - this.lastRefreshedUploadRate < 1000){ return; }

      // console.log('Rates:', this.uploadRates);

      if (this.uploadRates.length === 0){
        this.uploadRate = 0;
        return;
      }

      let totalRate = 0;
      let loadedSize = 0;
      for (var rate in this.uploadRates){
        totalRate += this.uploadRates[rate];
      }

      for (var size in this.uploadedSizes){
        loadedSize += this.uploadedSizes[size];
      }

      this.uploadRate = totalRate;
      this.uploadProgressSize = loadedSize < 0 ? 0 : loadedSize;
      // console.log('Upload Rate: ' + totalRate);
      // console.log('Uploading Size: ' + Math.round(loadedSize / 1048576) + ' MB');
    },


    async clearFormData(){
      this.cloudError = '';
      this.formError = {};
      this.formData = {};
    },

    async closeModal(){
      await this.clearFormData();
      this.modal = '';
    },


    async toggleSelectAllItems(){
      console.log('Toggle.');
      if (this.folders.length === 0 && this.files.length === 0){
        console.log('Nothing can be selected.');
        return;
      }

      let selectState = false;

      if (this.folders.length > 0){ this.folders[0].selected? selectState = true: selectState = false; }
      else { this.files[0].selected? selectState = true: selectState = false; }

      console.log('Setting everthing to', !selectState);

      for (let i=0; i < this.folders.length; i++){
        this.folders[i].selected = !selectState;
        console.log('Folder', this.folders[i].folderName, 'set to', !selectState);
      }

      for (let i=0; i < this.files.length; i++){
        this.files[i].selected = !selectState;
        console.log('File', this.files[i].fileName, 'set to', !selectState);
      }
    },

    async clearSelection() {
      this.folderSelection = [];
      this.fileSelection = [];
    },


    async resetUploadStats(){
      // Reset upload stats
      this.uploading = false;
      this.uploadSize = 0;
      this.requestSize = 0;
      this.uploadCount = 0;
      this.requestCount = 0;
      this.uploadRate = 0;

      console.log('Reset upload stats');
    },




  }
});
