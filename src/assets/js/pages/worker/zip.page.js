parasails.registerPage('zip', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    swSupported: true,
    swActive: false,

    sw: null,

    mcPort: null,
    mcReady: false,
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    //…
  },
  mounted: async function() {

    // Listen to message channel
    this.listenMessageChannel();

    // Install Service Worker
    while (this.swSupported){
      let swStatus = await this.registerServiceWorker();
      if (swStatus <= -1){
        this.swSupported = false;
        break;
      }
      if (swStatus === 0){
        this.swActive = true;
        break;
      }
      await new Promise(r => setTimeout(r,1000));
    }

    // Ping service worker
    this.swSupported? await this.pingSw(): null;

    this.keepAlive();
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    async registerServiceWorker(){

      let status = new Promise( res => {
        if ('serviceWorker' in navigator) {

          navigator.serviceWorker.getRegistration('./')
          .then( sw => {

            if (sw){
              this.sw = sw;
              res(0);
            }
    
            navigator.serviceWorker.register('/worker-zip.js', { scope: './' })
            .then((reg) =>{
          
              if(reg.installing) {
                console.log('Service worker installing');
                res(1);
              } else if(reg.waiting) {
                console.log('Service worker installed');
                res(2);
              } else if(reg.active) {
                console.log('Service worker active');
                this.sw = reg.active;
                res(0);
              }
          
            })
            .catch((error) => {
              // registration failed
              console.log('Worker registration failed: ' + error);
            });

          })
          .catch((error) => {
            // get registration failed
            console.log('Get worker registration failed: ' + error);
          });

          
        } else {
          // if service worker is not supported
          res(-1);
        }
      })

      return status;

    },

    async pingSw(){

      let res = await new Promise( r => {
        axios({
          method: 'GET',
          url: '/worker/zip/ping',
          withCredentials: false,
        })
        .then( res => {
          r(res);
        })
        .catch( err => {
          if (err) r(null);
        });

      });

      if (res){
        // Successfully setup worker
        // console.log(res.data);
        
        if (this.mcReady){
          this.mcPort.postMessage('state:ready');
        }
        return;
      }

      // Otherwise reload page
      location.reload();
  
    },

    keepAlive(){
      var ping = location.href.substr(0, location.href.lastIndexOf('/')) + '/ping';
      var interval = setInterval(() => {
        if (this.sw) {
          this.sw.postMessage('ping');
        } else {
          fetch(ping).then(res => res.text(!res.ok && clearInterval(interval)));
        }
        console.log('Staying alive...');
      }, 10000);
    },

    listenMessageChannel(){
      window.addEventListener('message', this.onMessage);
    },

    onMessage(e){
      console.log('[Port 2]:', e);

      if (e.data === 'hello'){
        this.mcPort = e.ports[0];
        this.mcReady = true;

        this.mcPort.postMessage('hello');
        return;
      }

      // Reply not ready if service worker is not ready or not supported
      if (!this.swReady || !this.swSupported){
        this.mcPort.postMessage('not ready');
        return;
      }

      // Reply to ready state check
      if (e.data.check){
        this.mcPort.postMessage({check: e.data.check});
      }

      // Process download request
      const data = e.data?.data;
      const name = e.data?.name;

      // If data or name do not exist, abort
      if (!data || !name){
        this.mcPort.postMessage('bad request');
        return;
      }



    }

  }
});
