self.addEventListener('install', () => {
  self.skipWaiting();
})

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
})


self.addEventListener('fetch', (event) => {
  const reqUrl =  event.request.url;

  if (reqUrl.endsWith('/ping')){
    return event.respondWith(new Response('pong'));
  }

  // if (reqUrl.endsWith('/cat')){
  //   const responseHeaders = new Headers({
  //     'Content-Type': 'application/octet-stream; charset=utf-8',
  //     'Content-Disposition': 'attachment',

  //     // Security headers
  //     'Content-Security-Policy': "default-src 'none'",
  //     'X-Content-Security-Policy': "default-src 'none'",
  //     'X-WebKit-CSP': "default-src 'none'",
  //     'X-XSS-Protection': '1; mode=block'
  //   })

  //   const url = 'https://d8d913s460fub.cloudfront.net/videoserver/cat-test-video-320x240.mp4';
  //   const res = await fetch(url);
  //   const stream = res.body;

  //   return event.respondWith(new Response(stream, { headers: responseHeaders }));
  // }

  // const url = new URL(reqUrl);
  // const zipRegExp = new RegExp(/^\/([a-zA-Z0-9]+)\/([a-zA-Z0-9]+)\/([^\/\\]+)$/, 'g');

  // if (url.hostname === 'dl.bitdocker.io'){
  //   let urlMatch = zipRegExp.exec(url.pathname);

  //   if (!urlMatch || urlMatch[1] !== 'zip' || urlMatch.length !== 4){
  //     return event.respondWith(fetch(event.request));
  //   }

  // }

})

self.onmessage = event => {

  // Respond to keep alive message
  if (event.data === 'ping'){ return; }

  
}



// ██╗  ██╗███████╗██╗     ██████╗ ███████╗██████╗ ███████╗
// ██║  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔════╝
// ███████║█████╗  ██║     ██████╔╝█████╗  ██████╔╝███████╗
// ██╔══██║██╔══╝  ██║     ██╔═══╝ ██╔══╝  ██╔══██╗╚════██║
// ██║  ██║███████╗███████╗██║     ███████╗██║  ██║███████║
// ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝╚══════╝
                                                        

function encodeFileName(name){
  let encoded = encodeURI(name);

  encoded = encoded.replace(/\#/g, '%23');
  encoded = encoded.replace(/\&/g, '%26');
  encoded = encoded.replace(/\+/g, '%2B');
  encoded = encoded.replace(/\,/g, '%2C');
  encoded = encoded.replace(/\?/g, '%3F');

  return encoded;
}

function decodeFileName(name){
  let decoded = decodeURI(name);

  decoded = decoded.replace(/\+/g, ' ');
  decoded = decoded.replace(/\%23/g, '#');
  decoded = decoded.replace(/\%24/g, '$');
  decoded = decoded.replace(/\%26/g, '&');
  decoded = decoded.replace(/\%2B/g, '+');
  decoded = decoded.replace(/\%2C/g, ',');
  decoded = decoded.replace(/\%3A/g, ':');
  decoded = decoded.replace(/\%3B/g, ';');
  decoded = decoded.replace(/\%3D/g, '=');
  decoded = decoded.replace(/\%3F/g, '?');
  decoded = decoded.replace(/\%40/g, '@');

  return decoded;
}
