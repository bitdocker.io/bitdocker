module.exports.cron = {
  // schedule: ['seconds', 'minutes', 'hours', 'dayOfMonth', 'month', 'dayOfWeek']

  removeExpiredTokens: {
    // Run every 30 mins
    schedule: '0 0/30 * * * *',
    start: true,
    onTick: async function () {

      // Remove tokens more than 20 mins old.
      await AuthToken.destroy({
        where: {
          createdAt: { '<': _.now() - 1200000 },
        }
      });

      // Remove download tokens more than 30 mins old.
      await DownloadToken.destroy({
        where: {
          createdAt: { '<': _.now() - 1800000 },
        }
      });
    }
  },

  updateStorageStats: {
    // Run every day at midnight AEST
    schedule: '0 0 0 * * * ',
    timezone: 'Australia/Sydney',
    start: false,
    onTick: async function() {

      // count total number of users
      // const userCount = await User.count();

      // Get day, month, year
      const datetime = new Date();
      const day = datetime.getDate();
      const month = datetime.getMonth() + 1;
      const year = datetime.getFullYear();

      // For each user, calculate the size of all files created
      await User.stream({})
      .eachRecord( async (user) => {

        var totalSize = 0;

        await FileObject.stream({ user: user.id })
        .eachRecord( async (file) => {
          totalSize += file.contentLength;
        });

        if (totalSize > 0){
          await StorageStats.createOne({
            user: user.id,
            day: day,
            month: month,
            year: year,
            bytesUsed: totalSize,
          });
        }

      });
    }
  }

};
