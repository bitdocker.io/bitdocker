const passport = require('passport');
const CustomStrategy = require('passport-custom').Strategy;
const axios = require('axios');
var jwt = require('jsonwebtoken');

passport.serializeUser((user, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  User.findOne({id}).exec( (err, user) => {

    if (err){
      sails.log.error(err);
      return cb('serverError', null);
    }
    if (!user){
      return cb('notFound', null);
    } else {
      return cb(null, user);
    }

  });
});

passport.use('just-id', new CustomStrategy( (req, cb) => {
  sails.log.debug('Just.ID Strategy called.');

  // Get jwt from cookie
  const jwtToken = req.query.token;
  if (!jwtToken){ return cb('unauthorized', false, 0, {message: 'unauthorized'}); }

  let token = '';

  try{
    const tokenDecode = jwt.verify(jwtToken, sails.config.custom.justIdPubKey);
    token = tokenDecode.token;
  } catch(err){
    sails.log.verbose('JWT invalid. Returning.');
    return cb('unauthorized', false, 0, {message: 'unauthorized'});
  }

  sails.log.debug('AXIOS GET:', sails.config.custom.ssoBaseUrl + '/api/v1/sso/verify');
  // contact just.id to verity user login token
  axios({
    method: 'GET',
    url: sails.config.custom.ssoBaseUrl + '/api/v1/sso/verify',
    params: {
      token: token,
    }
  })
  .then( (res) => {

    jwt.verify(res.data.jwt, sails.config.custom.justIdPubKey, (err, decoded) => {
      if (err){
        sails.log.error(err.message);
        sails.log.error('Failed decoding JWT token!');
        return cb('unauthorized', false, 0, {message: 'unauthorized'});
      }

      sails.log.debug('Decoded:', decoded);

      User.findOne({ uuid: decoded.uuid }).exec( (err, user) => {
        if (err) {
          sails.log.error(err.message);
          sails.log.error('Error fetching user after successful SSO!');
          return cb('serverError', false, 0, {message: 'serverError'});
        }

        if (!user){
          console.log('No existing user found. Creating one.');
          const newUUID = sails.helpers.uuidUser(decoded.emailAddress);

          sails.log.debug('New user UUID:', newUUID);

          User.create({
            emailAddress: decoded.emailAddress,
            fullName: decoded.fullName,
            emailStatus: decoded.emailStatus,
            password: sails.helpers.generateToken(),
            uuid: decoded.uuid,
            lastProfileUpdate: decoded.lastProfileUpdate,
          })
          .fetch()
          .exec( (err, user) => {
            if (err){
              sails.log.error(err.message);
              return cb('serverError', false, 0, {message: 'Token is valid, but error creating user.'});
            }

            return cb(null, user, decoded.exp, {message: 'Successfully signed up.'});
          });


        } else {
          sails.log.debug(user);

          // Reset ssoLogout flag if set
          if (user.ssoLogout){

            User.updateOne({ id: user.id })
            .set({ ssoLogout: false })
            .exec((err) => {
              if (err){
                sails.log.error('Error resetting ssoLogout flag!');
                return cb('serverError', false, 0, {message: 'serverError'});
              } else {
                sails.log.debug('SSO signin completed!');
                return cb(null, user, decoded.exp, { message: 'success'});
              }
            });

          } else {

            sails.log.debug('SSO signin completed!');
            return cb(null, user, decoded.exp, { message: 'success'});

          }

        }


      });

    });
  })
  .catch( (err) => {
    sails.log.error('SSO request failed:', err.message);
    return cb('serverError', false, 0, {message: 'serverError'});
  });


  // return cb(err, false, {message: 'serverError'});

  // return cb(null, false, {message: 'Invalid username or password.'});

  // return cb(null, userRecord, { message: 'Login Succesful'});


}));
