/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
  'GET /':                   { action: 'view-homepage-or-redirect' },
  'GET /welcome/:unused?':   { action: 'dashboard/view-welcome' },

  // 'GET /faq':                { action:   'view-faq' },
  // 'GET /legal/terms':        { action:   'legal/view-terms' },
  // 'GET /legal/privacy':      { action:   'legal/view-privacy' },
  // 'GET /contact':            { action:   'view-contact' },

  'GET /login':              { action: 'entrance/view-login' },

  'GET /account':            { action: 'account/view-account-overview' },
  'GET /account/password':   { action: 'account/view-edit-password' },
  'GET /account/profile':    { action: 'account/view-edit-profile' },

  'GET /folder/:id?':   { action: 'folder/view-user-folder-by-id' },
  'GET /share/:token':  { action: 'share/view-user-shared-link' },

  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗   ┬   ╔╦╗╔═╗╦ ╦╔╗╔╦  ╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗  ┌┼─   ║║║ ║║║║║║║║  ║ ║╠═╣ ║║╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝  └┘   ═╩╝╚═╝╚╩╝╝╚╝╩═╝╚═╝╩ ╩═╩╝╚═╝
  // '/terms':                   '/legal/terms',
  '/logout':                  '/api/v1/account/logout',

  '/worker/zip':    { action: 'worker/view-zip', locals: {layout: 'layouts/layout-min'} },

  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝
  // …


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the Parasails library, or by using those method names as the `action` in <ajax-form>.
  'GET /csrf': { action: 'security/grant-csrf-token' },

  '/api/v1/account/logout':                           { action: 'account/logout' },

  // 'POST  /api/v1/deliver-contact-form-message':          { action: 'deliver-contact-form-message' },
  'POST  /api/v1/observe-my-session':                 { action: 'observe-my-session', hasSocketFeatures: true },

  // ███████╗███████╗ ██████╗
  // ██╔════╝██╔════╝██╔═══██╗
  // ███████╗███████╗██║   ██║
  // ╚════██║╚════██║██║   ██║
  // ███████║███████║╚██████╔╝
  // ╚══════╝╚══════╝ ╚═════╝

  'GET /sso/just-id':   { action: 'sso/just-id' },
  'PUT /api/v1/sso/sync-user-profile':  { action: 'sso/sync-user-profile', csrf: false },
  'PUT /api/v1/sso/logout-user':  { action: 'sso/logout-user', csrf: false },


  // ███████╗██╗██╗     ███████╗███████╗
  // ██╔════╝██║██║     ██╔════╝██╔════╝
  // █████╗  ██║██║     █████╗  ███████╗
  // ██╔══╝  ██║██║     ██╔══╝  ╚════██║
  // ██║     ██║███████╗███████╗███████║
  // ╚═╝     ╚═╝╚══════╝╚══════╝╚══════╝


  'GET  /api/v1/file/get-download-params':    { action: 'file/get-download-params' },
  'POST /api/v1/file/get-upload-url':         { action: 'file/get-upload-url' },
  'POST /api/v1/file/finish-file-upload':     { action: 'file/finish-file-upload' },
  'PUT  /api/v1/file/remove-file':            { action: 'file/remove-file' },

  'POST /api/v1/file/start-large-file':           { action: 'file/start-large-file' },
  'POST /api/v1/file/get-upload-part-url':        { action: 'file/get-upload-part-url' },
  'POST /api/v1/file/finish-file-part-upload':    { action: 'file/finish-file-part-upload' },
  'POST /api/v1/file/finish-large-file-upload':   { action: 'file/finish-large-file-upload' },

  'GET  /api/v1/folder/get-folder-info-by-id':    { action: 'folder/get-folder-info-by-id' },
  'PUT  /api/v1/folder/remove-folder':            { action: 'folder/remove-folder' },
  'PUT  /api/v1/folder/update-folder':            { action: 'folder/update-folder' },
  'POST /api/v1/folder/create-folder':            { action: 'folder/create-folder' },



};
