module.exports = {

  friendlyName: 'Mark an user as super admin',

  description: 'Mark an user as super admin, givne an email address.',

  inputs: {
    emailAddress: {
      type: 'string',
      required: true,
      isEmail: true,
    }
  },

  fn: async function({emailAddress}){
    await User.updateOne({ emailAddress })
    .set({
      isSuperAdmin: true,
    });
  }

};
