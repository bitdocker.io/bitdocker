const assert = require('assert');

const TEST_CASES = [
  { filename: 'abc123.zip', valid: true},
  { filename: 'abc+123.zip', valid: false},
  { filename: 'abc 123.zip', valid: false},
  { filename: 'file/name.zip', valid: false},
  { filename: 'file&name.zip', valid: false},
  { filename: 'abc%20123.zip', valid: true},
  { filename: 'abc123 .zip', valid: false},
  { filename: 'abc%2B123.zip', valid: true},
  { filename: '%25.zip', valid: true},
  { filename: '%.zip', valid: false},
  { filename: '\0', valid: false},
  { filename: 'COM2', valid: false},
  { filename: 'CON', valid: false},
  { filename: '\n', valid: false},
  { filename: '\r', valid: false},
  { filename: '\u0000', valid: false},
  { filename: '\u001f', valid: false},
  { filename: '\u0080', valid: false},
  { filename: '\u007f', valid: false},
  { filename: '%7F', valid: true},
  { filename: '%E8%87%AA%E7%94%B1', valid: true},
];

describe('Helper (Check filename)', function(){

  describe('#checkFilename()', function(){

    it('should detect all not fully encoded filenames', async function(){

      for (let i=0; i < TEST_CASES.length; i++){
        const test = TEST_CASES[i];
        let checked = await sails.helpers.checkFilename(test.filename);

        assert( checked === test.valid, 'Filename [' + test.filename +'] should be ' + (test.valid? 'VALID' : 'INVALID') + '. Got ' + (checked? 'VALID' : 'INVALID') + ' instead.');
      }

    });
  });
});
