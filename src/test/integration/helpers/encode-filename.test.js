// var util = require('util');
const assert = require('assert');

const TEST_CASES = [
  {'fullyEncoded': '%20', 'minimallyEncoded': '+', 'string': ' '},
  {'fullyEncoded': '%21', 'minimallyEncoded': '!', 'string': '!'},
  {'fullyEncoded': '%22', 'minimallyEncoded': '%22', 'string': '"'},
  {'fullyEncoded': '%23', 'minimallyEncoded': '%23', 'string': '#'},
  {'fullyEncoded': '%24', 'minimallyEncoded': '$', 'string': '$'},
  {'fullyEncoded': '%25', 'minimallyEncoded': '%25', 'string': '%'},
  {'fullyEncoded': '%26', 'minimallyEncoded': '%26', 'string': '&'},
  {'fullyEncoded': '%27', 'minimallyEncoded': '\'', 'string': '\''},
  {'fullyEncoded': '%28', 'minimallyEncoded': '(', 'string': '('},
  {'fullyEncoded': '%29', 'minimallyEncoded': ')', 'string': ')'},
  {'fullyEncoded': '%2A', 'minimallyEncoded': '*', 'string': '*'},
  {'fullyEncoded': '%2B', 'minimallyEncoded': '%2B', 'string': '+'},
  {'fullyEncoded': '%2C', 'minimallyEncoded': '%2C', 'string': ','},
  {'fullyEncoded': '%2D', 'minimallyEncoded': '-', 'string': '-'},
  {'fullyEncoded': '%2E', 'minimallyEncoded': '.', 'string': '.'},
  {'fullyEncoded': '/', 'minimallyEncoded': '/', 'string': '/'},
  {'fullyEncoded': '%30', 'minimallyEncoded': '0', 'string': '0'},
  {'fullyEncoded': '%31', 'minimallyEncoded': '1', 'string': '1'},
  {'fullyEncoded': '%32', 'minimallyEncoded': '2', 'string': '2'},
  {'fullyEncoded': '%33', 'minimallyEncoded': '3', 'string': '3'},
  {'fullyEncoded': '%34', 'minimallyEncoded': '4', 'string': '4'},
  {'fullyEncoded': '%35', 'minimallyEncoded': '5', 'string': '5'},
  {'fullyEncoded': '%36', 'minimallyEncoded': '6', 'string': '6'},
  {'fullyEncoded': '%37', 'minimallyEncoded': '7', 'string': '7'},
  {'fullyEncoded': '%38', 'minimallyEncoded': '8', 'string': '8'},
  {'fullyEncoded': '%39', 'minimallyEncoded': '9', 'string': '9'},
  {'fullyEncoded': '%3A', 'minimallyEncoded': ':', 'string': ':'},
  {'fullyEncoded': '%3B', 'minimallyEncoded': ';', 'string': ';'},
  {'fullyEncoded': '%3C', 'minimallyEncoded': '%3C', 'string': '<'},
  {'fullyEncoded': '%3D', 'minimallyEncoded': '=', 'string': '='},
  {'fullyEncoded': '%3E', 'minimallyEncoded': '%3E', 'string': '>'},
  {'fullyEncoded': '%3F', 'minimallyEncoded': '%3F', 'string': '?'},
  {'fullyEncoded': '%40', 'minimallyEncoded': '@', 'string': '@'},
  {'fullyEncoded': '%41', 'minimallyEncoded': 'A', 'string': 'A'},
  {'fullyEncoded': '%42', 'minimallyEncoded': 'B', 'string': 'B'},
  {'fullyEncoded': '%43', 'minimallyEncoded': 'C', 'string': 'C'},
  {'fullyEncoded': '%44', 'minimallyEncoded': 'D', 'string': 'D'},
  {'fullyEncoded': '%45', 'minimallyEncoded': 'E', 'string': 'E'},
  {'fullyEncoded': '%46', 'minimallyEncoded': 'F', 'string': 'F'},
  {'fullyEncoded': '%47', 'minimallyEncoded': 'G', 'string': 'G'},
  {'fullyEncoded': '%48', 'minimallyEncoded': 'H', 'string': 'H'},
  {'fullyEncoded': '%49', 'minimallyEncoded': 'I', 'string': 'I'},
  {'fullyEncoded': '%4A', 'minimallyEncoded': 'J', 'string': 'J'},
  {'fullyEncoded': '%4B', 'minimallyEncoded': 'K', 'string': 'K'},
  {'fullyEncoded': '%4C', 'minimallyEncoded': 'L', 'string': 'L'},
  {'fullyEncoded': '%4D', 'minimallyEncoded': 'M', 'string': 'M'},
  {'fullyEncoded': '%4E', 'minimallyEncoded': 'N', 'string': 'N'},
  {'fullyEncoded': '%4F', 'minimallyEncoded': 'O', 'string': 'O'},
  {'fullyEncoded': '%50', 'minimallyEncoded': 'P', 'string': 'P'},
  {'fullyEncoded': '%51', 'minimallyEncoded': 'Q', 'string': 'Q'},
  {'fullyEncoded': '%52', 'minimallyEncoded': 'R', 'string': 'R'},
  {'fullyEncoded': '%53', 'minimallyEncoded': 'S', 'string': 'S'},
  {'fullyEncoded': '%54', 'minimallyEncoded': 'T', 'string': 'T'},
  {'fullyEncoded': '%55', 'minimallyEncoded': 'U', 'string': 'U'},
  {'fullyEncoded': '%56', 'minimallyEncoded': 'V', 'string': 'V'},
  {'fullyEncoded': '%57', 'minimallyEncoded': 'W', 'string': 'W'},
  {'fullyEncoded': '%58', 'minimallyEncoded': 'X', 'string': 'X'},
  {'fullyEncoded': '%59', 'minimallyEncoded': 'Y', 'string': 'Y'},
  {'fullyEncoded': '%5A', 'minimallyEncoded': 'Z', 'string': 'Z'},
  {'fullyEncoded': '%5B', 'minimallyEncoded': '%5B', 'string': '['},
  {'fullyEncoded': '%5C', 'minimallyEncoded': '%5C', 'string': '\\'},
  {'fullyEncoded': '%5D', 'minimallyEncoded': '%5D', 'string': ']'},
  {'fullyEncoded': '%5E', 'minimallyEncoded': '%5E', 'string': '^'},
  {'fullyEncoded': '%5F', 'minimallyEncoded': '_', 'string': '_'},
  {'fullyEncoded': '%60', 'minimallyEncoded': '%60', 'string': '`'},
  {'fullyEncoded': '%61', 'minimallyEncoded': 'a', 'string': 'a'},
  {'fullyEncoded': '%62', 'minimallyEncoded': 'b', 'string': 'b'},
  {'fullyEncoded': '%63', 'minimallyEncoded': 'c', 'string': 'c'},
  {'fullyEncoded': '%64', 'minimallyEncoded': 'd', 'string': 'd'},
  {'fullyEncoded': '%65', 'minimallyEncoded': 'e', 'string': 'e'},
  {'fullyEncoded': '%66', 'minimallyEncoded': 'f', 'string': 'f'},
  {'fullyEncoded': '%67', 'minimallyEncoded': 'g', 'string': 'g'},
  {'fullyEncoded': '%68', 'minimallyEncoded': 'h', 'string': 'h'},
  {'fullyEncoded': '%69', 'minimallyEncoded': 'i', 'string': 'i'},
  {'fullyEncoded': '%6A', 'minimallyEncoded': 'j', 'string': 'j'},
  {'fullyEncoded': '%6B', 'minimallyEncoded': 'k', 'string': 'k'},
  {'fullyEncoded': '%6C', 'minimallyEncoded': 'l', 'string': 'l'},
  {'fullyEncoded': '%6D', 'minimallyEncoded': 'm', 'string': 'm'},
  {'fullyEncoded': '%6E', 'minimallyEncoded': 'n', 'string': 'n'},
  {'fullyEncoded': '%6F', 'minimallyEncoded': 'o', 'string': 'o'},
  {'fullyEncoded': '%70', 'minimallyEncoded': 'p', 'string': 'p'},
  {'fullyEncoded': '%71', 'minimallyEncoded': 'q', 'string': 'q'},
  {'fullyEncoded': '%72', 'minimallyEncoded': 'r', 'string': 'r'},
  {'fullyEncoded': '%73', 'minimallyEncoded': 's', 'string': 's'},
  {'fullyEncoded': '%74', 'minimallyEncoded': 't', 'string': 't'},
  {'fullyEncoded': '%75', 'minimallyEncoded': 'u', 'string': 'u'},
  {'fullyEncoded': '%76', 'minimallyEncoded': 'v', 'string': 'v'},
  {'fullyEncoded': '%77', 'minimallyEncoded': 'w', 'string': 'w'},
  {'fullyEncoded': '%78', 'minimallyEncoded': 'x', 'string': 'x'},
  {'fullyEncoded': '%79', 'minimallyEncoded': 'y', 'string': 'y'},
  {'fullyEncoded': '%7A', 'minimallyEncoded': 'z', 'string': 'z'},
  {'fullyEncoded': '%7B', 'minimallyEncoded': '%7B', 'string': '{'},
  {'fullyEncoded': '%7C', 'minimallyEncoded': '%7C', 'string': '|'},
  {'fullyEncoded': '%7D', 'minimallyEncoded': '%7D', 'string': '}'},
  {'fullyEncoded': '%7E', 'minimallyEncoded': '~', 'string': '~'},
  {'fullyEncoded': '%7F', 'minimallyEncoded': '%7F', 'string': '\u007f'},
  {'fullyEncoded': '%E8%87%AA%E7%94%B1', 'minimallyEncoded': '%E8%87%AA%E7%94%B1', 'string': '\u81ea\u7531'},
  {'fullyEncoded': '%F0%90%90%80', 'minimallyEncoded': '%F0%90%90%80', 'string': '\ud801\udc00'}
];

const STRING_TEST_CASES = [
  {'decoded': 'abc 123.zip', 'encoded': 'abc%20123.zip'},
  {'decoded': 'abc+123.zip', 'encoded': 'abc%2B123.zip'},
  {'decoded': 'abc +123.zip', 'encoded': 'abc%20%2B123.zip'},
  {'decoded': '/user/0a7cfdec-c39f/pycam++-0.1.0.zip', 'encoded': '/user/0a7cfdec-c39f/pycam%2B%2B-0.1.0.zip'},
  {'decoded': '/user/0a7cfdec-c39f/New Folder/pycam++-0.1.0.zip', 'encoded': '/user/0a7cfdec-c39f/New%20Folder/pycam%2B%2B-0.1.0.zip'},
];

describe('Helper (Encoding/Decoding Filename)', function(){

  describe('#encodeFilename()', function(){

    it('should pass all the single character encoding tests', async function(){

      for (let i=0; i < TEST_CASES.length; i++){
        const TEST = TEST_CASES[i];
        let encoded = await sails.helpers.encodeFilename(TEST.string);

        assert.ok( encoded === TEST.fullyEncoded || encoded === TEST.minimallyEncoded, 'The string ' + TEST.string + ' should be encoded as [' + TEST.minimallyEncoded + '] or [' + TEST.fullyEncoded + ']. Got: [' + encoded + ']');
      }


    });

    it('should pass all the encoding string tests', async function(){

      for (let i=0; i < STRING_TEST_CASES.length; i++){
        const TEST = STRING_TEST_CASES[i];

        let encoded = await sails.helpers.encodeFilename(TEST.decoded);

        assert.ok(encoded === TEST.encoded, 'The string ' + TEST.encoded + ' should be encoded as [' + TEST.encoded + ']. Got: [' + encoded + ']');

      }

    });

  });

  describe('#decodeFilename()', function(){

    it('should pass all the single character decoding tests',  async function(){

      for (let i=0; i < TEST_CASES.length; i++){
        const TEST = TEST_CASES[i];

        // Fully encoded => decoded
        let fullDecoded = await sails.helpers.decodeFilename(TEST.fullyEncoded);

        assert.ok(fullDecoded === TEST.string, 'The fully encoded string ' + TEST.fullyEncoded + ' should be decoded as [' + TEST.string + ']. Got: [' + fullDecoded + ']');

        // Minimally encoded => decoded
        let minDecoded = await sails.helpers.decodeFilename(TEST.minimallyEncoded);

        assert.ok(minDecoded === TEST.string, 'The minimally encoded string ' + TEST.minimallyEncoded + ' should be decoded as [' + TEST.string + ']. Got: [' + minDecoded + ']');
      }


    });

    it('should pass all the decoding string tests', async function(){

      for (let i=0; i < STRING_TEST_CASES.length; i++){
        const TEST = STRING_TEST_CASES[i];

        let decoded = await sails.helpers.decodeFilename(TEST.encoded);

        assert.ok(decoded === TEST.decoded, 'The encoded string ' + TEST.encoded + ' should be decoded as [' + TEST.decoded + ']. Got: [' + decoded + ']');

      }

    });
  });


  describe('Encode-Decode Test', function(){

    it('should correctly decode an encoded string', async function(){

      for (let i=0; i < STRING_TEST_CASES.length; i++){
        const TEST = STRING_TEST_CASES[i];

        let decoded = await sails.helpers.decodeFilename(await sails.helpers.encodeFilename(TEST.decoded));

        assert.ok(decoded === TEST.decoded, 'The string ' + TEST.decoded + ' failed encode-decode test. Got: [' + decoded + ']');

      }

    });
  });

  describe('Decode-Encode Test', function(){

    it('should correctly encode a decoded string', async function(){

      for (let i=0; i < STRING_TEST_CASES.length; i++){
        const TEST = STRING_TEST_CASES[i];

        let encoded = await sails.helpers.encodeFilename(await sails.helpers.decodeFilename(TEST.encoded));

        assert.ok(encoded === TEST.encoded, 'The string ' + TEST.encoded + ' failed decode-encode test. Got: [' + encoded + ']');

      }

    });
  });

});
